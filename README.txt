                      **** README ****


## Development of the front-end and middle-end of a Tiger Compiler from scratch
## as described by Andrew W. Appel in his book "Modern Compiler Implementation".

# Devloped features:
• Lexical and Syntactic analysis.
• Building the Abstract Syntax Tree.
• Bindings Identifiers.
• Type Checking.
• Translating to the High Level Intermediate Representation.
# Used Languages and Technologies: C++ | YACC (Flex | Bison) | Python (Unit test) | Autotools.


suppressed lines, suppressed chunks, average chunks size:
.                             : 5490 409  13
    lib                       :  250  14  17
        misc                  :  250  14  17
            scoped-map.hh     :   71   2  35
            scoped-map.hxx    :  163   1 163
            symbol.hxx        :    6   4   1
            test-symbol.cc    :    1   1   1
            unique.hxx        :    8   5   1
            variant.hxx       :    1   1   1
    src                       : 5240 395  13
        ast                   : 1605  73  21
            array-exp.cc      :   26   1  26
            array-exp.hh      :   43   1  43
            array-exp.hxx     :   34   1  34
            assign-exp.cc     :   23   1  23
            assign-exp.hh     :   36   1  36
            assign-exp.hxx    :   23   1  23
            break-exp.cc      :   18   1  18
            break-exp.hh      :   30   1  30
            break-exp.hxx     :   12   1  12
            call-exp.cc       :   24   1  24
            call-exp.hh       :   43   1  43
            call-exp.hxx      :   34   1  34
            default-visitor.hxx:   20  11   1
            escapable.cc      :    8   1   8
            escapable.hh      :   30   1  30
            escapable.hxx     :   12   1  12
            field-var.cc      :   23   1  23
            field-var.hh      :   42   1  42
            field-var.hxx     :   34   1  34
            function-dec.hh   :    2   1   2
            if-exp.cc         :   26   1  26
            if-exp.hh         :   51   1  51
            if-exp.hxx        :   34   1  34
            let-exp.cc        :   23   1  23
            let-exp.hh        :   36   1  36
            let-exp.hxx       :   23   1  23
            method-call-exp.cc:   23   1  23
            method-call-exp.hh:   37   1  37
            method-call-exp.hxx:   23   1  23
            object-exp.cc     :   19   1  19
            object-exp.hh     :   30   1  30
            object-exp.hxx    :   12   1  12
            object-visitor.hxx:    9   5   1
            op-exp.cc         :   19   1  19
            pretty-printer.cc :  321   4  80
            pretty-printer.hh :   32   3  10
            record-exp.cc     :   25   1  25
            record-exp.hh     :   37   1  37
            record-exp.hxx    :   23   1  23
            record-ty.cc      :   22   1  22
            record-ty.hh      :   30   1  30
            record-ty.hxx     :   12   1  12
            seq-exp.cc        :   22   1  22
            seq-exp.hh        :   30   1  30
            seq-exp.hxx       :   12   1  12
            string-exp.cc     :   19   1  19
            string-exp.hh     :   30   1  30
            string-exp.hxx    :   12   1  12
            typable.cc        :    6   1   6
            typable.hh        :   30   1  30
            typable.hxx       :   11   1  11
            type-constructor.cc:    8   1   8
            type-constructor.hh:   30   1  30
            type-constructor.hxx:   11   1  11
        astclone              :  107  35   3
            cloner.cc         :   53  17   3
            cloner.cc~        :   54  18   3
        bind                  :  451  22  20
            binder.cc         :  158   7  22
            binder.hh         :   33   3  11
            binder.hxx        :   45   3  15
            libbind.cc        :   17   1  17
            libbind.hh        :   16   1  16
            renamer.cc        :   44   1  44
            renamer.hh        :    6   2   3
            renamer.hxx       :   43   2  21
            tasks.cc          :   43   1  43
            tasks.hh          :   46   1  46
        callgraph             :    4   1   4
            call-graph-visitor.cc:    4   1   4
        desugar               :  211   5  42
            bounds-checking-visitor.cc:  139   1 139
            bounds-checking-visitor.hh:    7   1   7
            desugar-visitor.cc:   64   2  32
            libdesugar.cc     :    1   1   1
        escapes               :   49   2  24
            escapes-visitor.cc:   35   1  35
            escapes-visitor.hh:   14   1  14
        inlining              :  197   7  28
            inliner.cc        :  119   1 119
            inliner.hh        :    7   2   3
            pruner.cc         :   60   2  30
            pruner.hh         :   11   2   5
        llvmtranslate         :  256  42   6
            escapes-collector.cc:   39   8   4
            llvm-type-visitor.cc:    5   4   1
            translator.cc     :  212  30   7
        object                :  401  71   5
            binder.cc         :   12   2   6
            desugar-visitor.cc:  105  37   2
            libobject.cc      :    8   2   4
            libobject.hh      :    5   1   5
            overfun-binder.cc :   11   1  11
            overfun-binder.hh :   19   1  19
            overfun-type-checker.cc:    4   1   4
            overfun-type-checker.hh:   15   1  15
            renamer.cc        :   44   8   5
            tasks.cc          :   17   2   8
            tasks.hh          :    4   1   4
            type-checker.cc   :  157  14  11
        overload              :  159   3  53
            binder.cc         :    4   1   4
            type-checker.cc   :  139   1 139
            type-checker.hh   :   16   1  16
        parse                 :  459  12  38
            parsetiger.yy     :  432   7  61
            scantiger.ll      :   18   3   6
            tasks.cc          :    2   1   2
            tiger-parser.cc   :    7   1   7
        temp                  :   57  16   3
            identifier.hxx    :   57  16   3
        translate             :  304  38   8
            exp.cc            :   47   6   7
            level.cc          :    3   1   3
            translation.cc    :  148  10  14
            translation.hh    :    6   1   6
            translator.cc     :  100  20   5
        tree                  :   54   1  54
            fragment.cc       :   54   1  54
        type                  :  926  67  13
            array.cc          :   11   1  11
            array.hh          :   25   1  25
            array.hxx         :   10   1  10
            attribute.hxx     :    1   1   1
            builtin-types.cc  :   69   1  69
            builtin-types.hh  :   91   1  91
            class.cc          :   48   6   8
            class.hh          :    2   1   2
            default-visitor.hxx:   11   5   2
            function.cc       :   39   1  39
            function.hh       :    2   1   2
            method.cc         :   11   1  11
            method.hh         :    2   1   2
            named.cc          :   33   2  16
            named.hxx         :    2   1   2
            nil.cc            :    2   1   2
            pretty-printer.cc :   55   7   7
            record.cc         :   28   2  14
            record.hh         :    1   1   1
            type-checker.cc   :  448  22  20
            type-checker.hh   :   13   2   6
            type-checker.hxx  :   20   5   4
            type.hxx          :    2   2   1
