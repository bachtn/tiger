#!/usr/bin/env python3.5
import os
import argparse
import glob

def generate_css():
  f = open('tsuit_css_file.css', 'w')
  css_code = """
  body
  {
    background-color:#DCDCDC 
  }

  .TC
  {
    width: 400px;
    height: 250px;
    text-align: justify;
    border: 3px solid grey;
    overflow: auto;
    display: inline-block;
  }

  h1
  {
    color: black;
  }

  .introduction, .about
  {
    color: black;
    font-family: Georgia, sans-serif;
    font-weight: bold;
    font-size: 20px;
    text-align: center;
  }

  #gif1
  {
    float: right; 
  }

  strong
  {
    font-size: 22px;
  }

  #t1_2, #t2_2, #t3_2
  {
    color: lime;
  }

  #t1_3, #t2_3, #t3_3
  {
    color: red;
  }"""

  f.write(css_code)
  f.close()

def generate_html(t1_1, t1_2, t1_3, t1_5,
                  t2_1, t2_2, t2_3, t2_5,
                  t3_1, t3_2, t3_3, t3_5):
  f = open('tsuit_html_file.html', 'w')
  html_code = """
  <html>
    <head>
      <meta charset="utf-8" />  </head>
      <link rel="stylesheet" href="tsuit_css_file.css"/>
      <title>Tiger Compiler Test Suit</title>
    <body>
      <h1>Tiger Project</h1>
        <p class= "introduction">
          Tiger Compiler test suit. Results of the tests made for TC-1, TC-2 and TC-3.
        </p>
        <p>
        <img
        src="http://angeoudemongif.a.n.pic.centerblog.net/2cba995f.gif"
        border="0"  alt="Image du Blog angeoudemongif.centerblog.net" id="gif1" />
        <br/><br/><br/>
        </p>
      <h2>Test list:</h2>
          <div class = "TC">
            <h2>Testing TC-1</h2>
              <p id="t1_1"> Number of tested files: """ + str(t1_1) + """<br /></p>
              <p id="t1_2"> Passed tests: """ + str(t1_2) + """<br /></p>
              <p id="t1_3"> Failed tests: """ + str(t1_3) + """<br /></p>
              <p id="t1_5"> Statistics: """ + str(t1_5) + """"<br /></p>
          </div>
          <div class = "TC">
            <h2>Testing TC-2</h2>
              <p id="t2_1"> Number of tested files: """ + str(t2_1) + """<br /></p>
              <p id="t2_2"> Passed tests: """ + str(t2_2) + """<br /></p>
              <p id="t2_3"> Failed tests: """ + str(t2_3) + """<br /></p>
              <p id="t2_5"> Statistics: """ + t2_5 + """<br /></p>
            </div>
          <div class = "TC">
            <h2>Testing TC-3</h2>
              <p id="t3_1"> Number of tested files: """ + str(t3_1) + """<br /></p>
              <p id="t3_2"> Passed tests: """ + str(t3_2) + """<br /></p>
              <p id="t3_3"> Failed tests: """ + str(t3_3) + """<br /></p>
              <p id="t3_5"> Statistics: """ + str(t3_5) + """<br /></p>
            </div>
      <h2>About:</h2>
        <p class="about">
          "By bakhti_a (chief), pochez_a and mendoz_r".
        </p>
    </body>
  </html>"""

  f.write(html_code)
  f.close()
