#!/usr/bin/env python 3.5

import os
import argparse
import glob
import subprocess
import errno
import shutil

nbr_tested_files_1 = 0
nbr_tested_files_2 = 0
nbr_tested_files_3 = 0
tc1_fail = 0
tc2_fail = 0
tc3_fail = 0

class bcolors:
    CYAN = '\033[96m'
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    WARNING = '\033[93m'
    OKGREEN = '\033[92m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def create_dir(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

def print_summary():
    print(bcolors.BOLD + bcolors.HEADER + '*_* Summary *_*')
    print(bcolors.CYAN + '*_* TC 1 :')
    print(bcolors.OKGREEN + 'Number of tested files: %s' % str(nbr_tested_files_1))
    if (tc1_fail == 0):
        print(bcolors.OKGREEN + ' -> PASS')
    else:
        print(bcolors.FAIL + ' -> FAIL : ' + str(tc1_fail))
    print(bcolors.CYAN + '*_* TC 2 :')
    print(bcolors.OKGREEN + 'Number of tested files: %s' % str(nbr_tested_files_2))
    if (tc2_fail == 0):
        print(bcolors.OKGREEN + ' -> PASS')
    else:
        print(bcolors.FAIL + ' -> FAIL : ' + str(tc2_fail))
    print(bcolors.CYAN + '*_* TC 3 :')
    print(bcolors.OKGREEN + 'Number of tested files: %s' % str(nbr_tested_files_3))
    if (tc3_fail == 0):
        print(bcolors.OKGREEN + ' -> PASS')
    else:
        print(bcolors.FAIL + ' -> FAIL : ' + str(tc3_fail))

def print_result(err_nbr, nbr_file, log):
    print(bcolors.OKGREEN + 'Number of tested files: %s' % str(nbr_file))
    if (err_nbr == 0):
        print(bcolors.OKGREEN + ' -> PASS')
    else:
        print(bcolors.FAIL + ' -> FAIL : %s%s' % (str(err_nbr), log))

def tc1_testing(exec_path, src_path, directory, tc_index, return_code, dir_name, obj):
   err = 0
   nbr_file = 0
   log = """"""
   print(bcolors.OKGREEN + '***  Testing file \'%s\'' % dir_name)
   for filename in glob.glob(os.path.join('%s/%s' % (src_path, directory), '*.tig')):
      nbr_file += 1;
      if (obj == 1):
         cmd = [exec_path, '--object', '--parse', filename]
      else:
         cmd = [exec_path, filename]
      child = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      output = child.communicate()[0]
      a = child.returncode
      if (str(a) != str(return_code)):
         if (err == 0):
            log = """%s\n%s expected: %s, my: %s""" % (log, str(filename), str(return_code), str(a))
            err = 1
         else:
            log = """%s\n%s expected: %s, my: %s""" % (log, str(filename), str(return_code), str(a))
            err+= 1
   global nbr_tested_files_1
   nbr_tested_files_1 += nbr_file
   global tc1_fail
   tc1_fail += err
   if (err != 0):
      print_result(err, nbr_file, log)


def run(cmd, log_file):
   child = subprocess.Popen(cmd, shell=True, universal_newlines=True, stdout=log_file)
   child.wait()
   return child


def tc2_testing(exec_path, src_path, directory, tc_index, return_code, dir_name, obj):
   err = 0
   nbr_file = 0
   log = """"""
   os.system('chmod -R 777 %s' % src_path)
   print(bcolors.OKGREEN + '***  Testing file \'%s\'' % dir_name)
   for filename in glob.glob(os.path.join('%s/%s' % (src_path, directory), '*.tig')):
      nbr_file += 1;
      path1 = '%s/tmp1.tig' % (src_path, directory)
      path2 = '%s/tmp2.tig' % (src_path, directory)
      if (obj == 1):
        child1 = subprocess.Popen("%s --object -AD %s > %s" %(exec_path, filename, path1),
          shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output = child1.communicate()[0]
        a = child1.returncode
        child2 = subprocess.Popen("%s --object -AD %s | %s  --object -AD - > %s"
          %(exec_path, filename, exec_path, path2), shell=True,
          stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output = child2.communicate()[0]
        b = child2.returncode
      else:
        child1 = subprocess.Popen("%s -AD %s > %s" %(exec_path, filename, path1),
          shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output = child1.communicate()[0]
        a = child1.returncode
        child2 = subprocess.Popen("%s -AD %s | %s  -AD - > %s"
          %(exec_path, filename, exec_path, path2), shell=True,
          stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output = child2.communicate()[0]
        b = child2.returncode
      if (str(a) != str(return_code) or str(b) != str(return_code)):
          if (err == 0):
              log = """%s\n%s expected: %s, my: a = %s, b = %s""" % (log, str(filename), str(return_code), str(a), str(b))
              err = 1;
          else:
              log = """%s\n%s expected: %s, my: a = %s, b = %s""" % (log, str(filename), str(return_code), str(a), str(b))
              err+= 1
      child3 = subprocess.Popen("diff %s %s" %(path1, path2),
        shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      output = child3.communicate()[0]
      c = child3.returncode
      if (c != 0):
          if (err == 0):
              log = """%s\n%s Diff failed""" % (log, str(filename))
              err = 1;
          else:
              log = """%s\n%s Diff failed""" % (log, str(filename))
              err+= 1
      os.remove(path1)
      os.remove(path2)
   global nbr_tested_files_2
   nbr_tested_files_2 += nbr_file
   global tc2_fail
   tc2_fail += err
   if (err != 0):
      print_result(err, nbr_file, log)


def tc3_testing(exec_path, src_path, directory, tc_index, return_code, dir_name, obj):
   err = 0
   nbr_file = 0
   log = """"""
   print(bcolors.OKGREEN + '***  Testing file \'%s\'' % dir_name)
   for filename in glob.glob(os.path.join('%s/%s' % (src_path, directory), '*.tig')):
      nbr_file += 1;
      if (obj == 1):
         cmd = [exec_path, '--object', '--object-bindings-compute', '--ast-display', filename]
      elif (str(dir_name) == 'renamer'):
         cmd = [exec_path, '--rename', '-A', filename]
      elif (str(dir_name) == 'escape'):
         cmd = [exec_path, '-EA', '-eEA', filename]
      else:
         cmd = [exec_path, '-bBA', filename]
      child = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      output = child.communicate()[0]
      a = child.returncode
      if (str(a) != str(return_code)):
         if (err == 0):
            log = """%s\n%s expected: %s, my: %s""" % (log, str(filename), str(return_code), str(a))
            err = 1
         else:
            err+= 1
            log = """%s\n%s expected: %s, my: %s""" % (log, str(filename), str(return_code), str(a))
   global nbr_tested_files_3
   nbr_tested_files_3 += nbr_file
   global tc3_fail
   tc3_fail += err
   if (err != 0):
      print_result(err, nbr_file, log)


def file_test(exec_path, src_path, tc_index, x):
    for directory in os.listdir(src_path):
      if os.path.isdir(os.path.join(src_path, directory)) and str(directory) != 'log_file' and str(directory) != '__pycache__':
          return_code = str(directory)[len(str(directory)) - x]
          dir_name = directory[:-3]
          obj = 0
          if (str(directory)[0] == '1'):
            obj = 1
          if (tc_index == 1):
             tc1_testing(exec_path, src_path, directory, tc_index, return_code, dir_name, obj)
          elif (tc_index == 2):
             if (str(return_code) == '0'):
                 tc2_testing(exec_path, src_path, directory, tc_index, return_code, dir_name, obj)
          elif (tc_index == 3):
             tc3_testing(exec_path, src_path, directory, tc_index, return_code, dir_name, obj)

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('-ep', '--expath', action='store', dest='exec_path',
      help="The path of the executable")
  parser.add_argument('-sp', '--srcpath', action='store', dest='src_path',
      help="The path of the source files")
  path = parser.parse_args()
  exec_path = path.exec_path
  src_path = path.src_path
  global nbr_tested_files
  print(bcolors.HEADER + " ===    Testing TC1     === ")
  file_test(exec_path, src_path, 1, 3)
  print(bcolors.HEADER + " ===    Testing TC2     === ")
  file_test(exec_path, src_path, 2, 2)
  print(bcolors.HEADER + " ===    Testing TC3     === ")
  file_test(exec_path, src_path, 3, 1)
  print_summary()
  print(bcolors.ENDC)

if __name__ == '__main__':
    main()
