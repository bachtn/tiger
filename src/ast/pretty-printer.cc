/**
 ** \file ast/pretty-printer.cc
 ** \brief Implementation of ast::PrettyPrinter.
 */

#include <sstream>
#include <misc/escape.hh>
#include <misc/indent.hh>
#include <misc/separator.hh>
#include <misc/pair.hh>
#include <ast/all.hh>
#include <ast/pretty-printer.hh>
#include <ast/libast.hh>
#include <type/class.hh>

bool in_class = false;

namespace ast
{
  template <typename Colorified>
  std::string PrettyPrinter::
  color (bool colored, int color_choice, Colorified color_this)
  {
    std::string color_ostr;
    std::stringstream ss;
    if (colored)
      ss << "\e[" << color_choice << "m" << color_this << "\e[0m";
    else
      ss << color_this;
      color_ostr = ss.str();
    return color_ostr;
  }

  // Anonymous namespace: these functions are private to this file.
  namespace
  {
    /// Output \a e on \a ostr.
    inline
    std::ostream&
    operator<< (std::ostream& ostr, const Escapable& e)
    {
      if (escapes_display(ostr)
  // FIXME: Some code was deleted here.
          && e.escapable_get()
          )
        ostr << "/*"
             << " escaping "
             << "*/ ";

      return ostr;
    }

    /// \brief Output \a e on \a ostr.
    ///
    /// Used to factor the output of the name declared,
    /// and its possible additional attributes.
    inline
    std::ostream&
    operator<< (std::ostream& ostr, const Dec& e)
    {
      ostr << e.name_get();
      if (bindings_display(ostr))
        ostr << " /* "
             << &e << " */";
      return ostr;
    }
  }

  PrettyPrinter::PrettyPrinter(std::ostream& ostr)
    : ostr_(ostr)
  {}


  void
  PrettyPrinter::operator()(const SimpleVar& e)
  {
    ostr_ << e.name_get();
    if (bindings_display(ostr_) /* && !in_class */)
      ostr_ << color(color_printer, 93, " /* ")
            << e.def_get() << color(color_printer, 93, " */");
  }

  /* obj.size */
  void
  PrettyPrinter::operator()(const FieldVar& e)
  {
    ostr_ << e.var_get() << color(color_printer, 93, '.') << e.id_name_get();
  }

  /* Foo[10]. */
  void
  PrettyPrinter::operator()(const SubscriptVar& e)
  {
    ostr_ << e.var_get() << color(color_printer, 93, '[') << misc::incindent << e.index_get()
          << misc::decindent << color(color_printer, 93, ']');
  }

  void
  PrettyPrinter::operator()(const CastVar& e)
  {
    ostr_ << color(color_printer, 96, "_cast")
          <<color(color_printer, 93, "(") << e.var_get()
          << color(color_printer, 93, ", ") << e.ty_get()
          << color(color_printer, 93, ')');
  }

  /*            Dec           */
  void
  PrettyPrinter::operator()(const Dec& e)
  {
    ostr_ << e.name_get();
  }

  /* Function dec */
  void
  PrettyPrinter::operator()(const FunctionDec& e)
  {
    bool tmp_in_class = in_class;
    in_class = false;
    if (e.body_get())
      print_function<FunctionDec>(e, color(color_printer, 92, "function"));
    else
      print_function<FunctionDec>(e, color(color_printer, 96, "primitive"));
    in_class = tmp_in_class;
  }

  /* Function dec */
  void
  PrettyPrinter::operator()(const MethodDec& e)
  {
    bool tmp_in_class = in_class;
    in_class = false;
    print_function<MethodDec>(e, color(color_printer, 96, "method"));
    in_class = tmp_in_class;
  }
  /* type b = ty */
  void
  PrettyPrinter::operator()(const TypeDec& e)
  {
    ostr_ << color(color_printer, 96, "type ")
          << color(color_printer, 96, e.name_get());
    if (bindings_display(ostr_) /* && !in_class */)
      ostr_ << color(color_printer, 93, " /* ")
            << &e << color(color_printer, 93, " */");
    ostr_ << color(color_printer, 93, " = ")
          << e.ty_get() << misc::iendl;
  }

  /* var a [: int] :=  exp */
  void
  PrettyPrinter::operator()(const VarDec& e)
  {
    if (!e.init_get())
    {
      if (e.escapable_get() && escapes_display(ostr_))
        ostr_ << "/* escaping */ ";
      ostr_ << e.name_get();
      if (bindings_display(ostr_) && !in_class)
        ostr_ << " /* " << &e << " */";
      ostr_  << color(color_printer, 93, " : ") << *(e.type_name_get());
    }
    else
    {
      if (!e.type_name_get())
      {
        ostr_ << color(color_printer, 96, "var ");
        if (e.escapable_get() && escapes_display(ostr_))
          ostr_ << color(color_printer, 93, "/*")
                << " escaping "
                << color(color_printer, 93, "*/ ");
        ostr_ << e.name_get();
        if (bindings_display(ostr_) && !in_class)
          ostr_ << color(color_printer, 93, " /* ")
                << &e
                << color(color_printer, 93, " */");
        ostr_ << color(color_printer, 93, " := ") << *(e.init_get())
              << misc::iendl;
      }
      else
      {
         ostr_ << color(color_printer, 96, "var ");
         if (e.escapable_get() && escapes_display(ostr_))
           ostr_ << "/* escaping */ ";
         ostr_ << e.name_get();
         if (bindings_display(ostr_) && !in_class)
           ostr_ << color(color_printer, 93, " /* ")
                 << &e << color(color_printer, 93, " */");
         ostr_ << color(color_printer, 93, " : ")
               << *(e.type_name_get())
               << color(color_printer, 93, " := ")
               << *(e.init_get()) << misc::iendl;
      }
    }

  }


  /*            EXP         */
  /* tab[10] of 0 */
  void
  PrettyPrinter::operator()(const ArrayExp& e)
  {
    ostr_ << e.type_name_get() << color(color_printer, 93, "[")
          << e.size_get()      << color(color_printer, 93, "]")
          << color(color_printer, 96, " of ")
          << e.init_get();
  }

  /* a := 2 */
  void
  PrettyPrinter::operator()(const AssignExp& e)
  {
    ostr_ << e.var_get() << color(color_printer, 93, " := ") << e.value_get();
  }

  /* break */
  void
  PrettyPrinter::operator()(const BreakExp& e)
  {
    ostr_ << color(color_printer, 96, "break");
    if (bindings_display(ostr_) /* && !in_class */)
      ostr_ << color(color_printer, 93, " /* ")
            << e.def_get() << color(color_printer, 93, " */");
  }

  /* fact(5, 10) */
  void
  PrettyPrinter::operator()(const CallExp& e)
  {
    ostr_ << e.name_get();
    if (bindings_display(ostr_) /* && !in_class */)
      ostr_ << color(color_printer, 93, " /* ")
            << e.def_get() << color(color_printer, 93, " */");
    ostr_ << color(color_printer, 93, " (")
          << misc::separate(e.arg_list_get(), color(color_printer, 93, ","))
          << color(color_printer, 93, ")");
  }

  /* obj.fact(5, 10) */
  void
  PrettyPrinter::operator()(const MethodCallExp& e)
  {
    ostr_ << e.object_get() << color(color_printer, 93, ".") << e.name_get();
    if (bindings_display(ostr_) && !in_class)
      ostr_ << color(color_printer, 93, " /* ")
            << e.def_get() << color(color_printer, 93, " */");
    ostr_ << color(color_printer, 93, "(")
          << misc::separate(e.arg_list_get(), color(color_printer, 93, ","))
          << color(color_printer, 93, ")");
  }

  /*
    for i := 5 to 8 do
      fact(5)
  */
  void
  PrettyPrinter::operator()(const ForExp& e)
  {
    ostr_ << color(color_printer, 92, "for ");
    if (bindings_display(ostr_) /* && !in_class */)
      ostr_ << " /* " << &e << " */";
    if (e.vardec_get().escapable_get() && escapes_display(ostr_))
      ostr_ << "/* escaping */";
    ostr_ << " " << static_cast<const Dec&>(e.vardec_get())
          << color(color_printer, 93, " := ")
          << *(e.vardec_get().init_get())
          << color(color_printer, 96, " to ") << e.hi_get()
          << color(color_printer, 96, " do")  << misc::incendl
          << e.body_get() << misc::decindent;
  }

  /*
    if a then
      b
    else
      c
  */
  void
  PrettyPrinter::operator()(const IfExp& e)
  {
    ostr_   << color(color_printer, 96, "if ") << e.cond_get()
            << color(color_printer, 96, " then") << misc::incendl
            << e.then_exp_get()    << misc::decendl;
    if (e.else_exp_get())
      ostr_ << color(color_printer, 96, "else") << misc::incendl
            << *(e.else_exp_get()) <<  misc::decendl;
  }

  /* 10 */
  void
  PrettyPrinter::operator()(const IntExp& e)
  {
    ostr_ << e.value_get();
  }
  /*
     let
        var a := 5
     in
        print(a)
      end
  */
  void
  PrettyPrinter::operator()(const LetExp& e)
  {
    ostr_ << color(color_printer, 92, "let") << misc::incendl
          << e.let_get()    << misc::decendl
          << color(color_printer, 92, "in")  << misc::incendl
          << e.in_get()     << misc::decendl
          << color(color_printer, 92, "end") << misc::iendl;
  }

  /* nil */
  void
  PrettyPrinter::operator()(const NilExp&)
  {
    ostr_ << color(color_printer, 96, "nil");
  }

  /* new object */
  void
  PrettyPrinter::operator()(const ObjectExp& e)
  {
    ostr_ << color(color_printer, 96, "new ") << e.type_name_get();
  }

  /* a + b */
  void
  PrettyPrinter::operator()(const OpExp& e)
  {
    ostr_ << color(color_printer, 93, "(") << e.left_get() << " "
          << str(e.oper_get()) << " "
          << e.right_get() << color(color_printer, 93, ")");
  }

  /* my_record { value1 = a, value2 = b } */
  void
  PrettyPrinter::operator()(const RecordExp& e)
  {
    ostr_ << e.type_name_get();
    print_record<RecordExp>(e);
  }

  /*
    a := 1;
    b := 2;
    print(a + b);
  */
  void
  PrettyPrinter::operator()(const SeqExp& e)
  {
    if (e.exps_get().size() == 1)
      ostr_
        << misc::separate
        (e.exps_get(), misc::make_pair<>
         (color(color_printer, 93, ";"), misc::iendl));
    else
    {
      ostr_ << color(color_printer, 93, "(");
      if (e.exps_get().size() >= 2)
        ostr_ << misc::incendl
              << misc::separate
          (e.exps_get(), misc::make_pair<>
           (color(color_printer, 93, ";"), misc::iendl))
          << misc::decendl << color(color_printer, 93, ")");
      else
        ostr_
         << misc::separate
          (e.exps_get(), misc::make_pair<>
           (color(color_printer, 93, ";"), misc::iendl))
         << color(color_printer, 93, ")");
    }
  }

  /* "hello\n" */
  void
  PrettyPrinter::operator()(const StringExp& e)
  {
    ostr_ << color(color_printer, 93, "\"") << misc::escape(e.str_get())
          << color(color_printer, 93, "\"");
  }

  /*
    while true do
      print(a + b)
  */
  void
  PrettyPrinter::operator()(const WhileExp& e)
  {
    ostr_ << color(color_printer, 96, "while");
    if (bindings_display(ostr_) /* && !in_class */)
      ostr_ << color(color_printer, 93, " /* ")
            << &e << color(color_printer, 93, " */");
    ostr_ << color(color_printer, 93, " (")
          << e.test_get()
          << color(color_printer, 93, ") ")
          <<"do"         << misc::incendl
          << e.body_get() << misc::decindent;
  }

  /**/
  void
  PrettyPrinter::operator()(const CastExp& e)
  {
    ostr_ << color(color_printer, 96, "_cast")
          << color(color_printer, 93, "(")
          << e.exp_get() << color(color_printer, 93, ", ")
          << e.ty_get() << color(color_printer, 93, ')');
  }

  /*        TY      */
  /* array of int */
  void
      PrettyPrinter::operator()(const ArrayTy& e)
  {
    ostr_ << color(color_printer, 96, "array of ")
          << e.base_type_get();
  }

  /* classTy */
  void
  PrettyPrinter::operator()(const ClassTy& e)

  {
    in_class = true;
    ostr_ << color(color_printer, 96, "class ");
    if (&(e.super_get()))
      ostr_ << color(color_printer, 96, "extends ")
            << e.super_get() << " ";
    ostr_ << color(color_printer, 93, "{ ")
          << e.decs_get() << color(color_printer, 93, " }");
    in_class = false;
  }

  /* int */
  void
  PrettyPrinter::operator()(const NameTy& e)
  {
    ostr_
      << color(color_printer, 96, e.name_get());
    if (bindings_display(ostr_) /* && !in_class */)
      ostr_ << color(color_printer, 93, " /* ")
            << e.def_get() << color(color_printer, 93, " */");
  }

  /* { index : int, value : string } */
  void
  PrettyPrinter::operator()(const RecordTy& e)
  {
    print_record<RecordTy>(e);
  }

  /* decs-list */
  void

  PrettyPrinter::operator()(const DecsList& e)
  {
    ostr_ << misc::separate(e.decs_get(), "");
  }

  /* id : type-id */
  void
  PrettyPrinter::operator()(const Field& e)
  {
    ostr_ << e.name_get() << color(color_printer, 93, " : ")
          << e.type_name_get();
  }

  /* id = exp */

  void
  PrettyPrinter::operator()(const FieldInit& e)
  {
    ostr_ << e.name_get()
          << color(color_printer, 93, " = ")
          << e.init_get();
  }



  template <typename RecordClass>
  void PrettyPrinter::print_record(const RecordClass& e)
  {
    ostr_ << " {" << misc::separate(e.ft_get(), color(color_printer, 93, ", "))
          << color(color_printer, 93, "}");
  }

  template <typename FunctionClass>
  void PrettyPrinter::print_function(const FunctionClass& e, std::string str)
  {
    if (str == "function")
      ostr_ << color(color_printer, 92, str) << " " << e.name_get();
    else
      ostr_ << str << " " << e.name_get();
    if (bindings_display(ostr_) && str != "method")
      ostr_ << color(color_printer, 93, " /* ") << &e
            << color(color_printer, 93, " */");
    ostr_ << color(color_printer, 93, " (");
    ostr_ << misc::separate
      (e.formals_get().decs_get(), color(color_printer, 93, ", "))
          << color(color_printer, 93, ")");
    if (e.result_get())
      ostr_ << color(color_printer, 93, " : ")
            << *(e.result_get());
    if (e.body_get())
      ostr_ << color(color_printer, 93, " = ")
            << misc::incendl
            << *(e.body_get())
            << misc::decendl;
    else
      ostr_ << misc::iendl;
  }
} // namespace as
