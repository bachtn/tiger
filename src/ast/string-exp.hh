/**
 ** \file ast/string-exp.hh
 ** \brief Declaration of ast::StringExp.
 */

#pragma once

#include <ast/exp.hh>
#include <string>

namespace ast
{

  /// StringExp.
  class StringExp : public Exp
  {
  public:
    StringExp(const Location& location, std::string str);
    virtual ~StringExp();
    virtual void accept(ConstVisitor& v) const override;
    virtual void accept(Visitor& v) override;
    const std::string& str_get() const;
    std::string& str_get();
  protected:
    std::string str_;
  };

} // namespace ast

#include <ast/string-exp.hxx>

