/**
 ** \file ast/if-exp.hh
 ** \brief Declaration of ast::IfExp.
 */

#pragma once

#include <ast/exp.hh>
# include <ast/seq-exp.hh>

namespace ast
{

  /// IfExp.
  class IfExp : public Exp
  {
    public:
      IfExp(const Location& location, Exp* cond, Exp* then_exp, Exp* else_exp);
      IfExp(const Location& location, Exp* cond, Exp* then_exp);
      virtual ~IfExp();

    public:
      virtual void accept(ConstVisitor& v) const override;
      virtual void accept(Visitor& v) override;

    public:
      const Exp& cond_get() const;
      Exp& cond_get();
      const Exp& then_exp_get() const;
      Exp& then_exp_get();
      const Exp* else_exp_get() const;
      Exp* else_exp_get();

    protected:
     Exp* cond_;
     Exp* then_exp_;
     Exp* else_exp_;
  };

} // namespace ast

#include <ast/if-exp.hxx>
