/**
 ** \file ast/if-exp.hxx
 ** \brief Inline methods of ast::IfExp.
 */

#pragma once

#include <ast/if-exp.hh>

namespace ast
{
  inline const Exp&
  IfExp::cond_get() const
  {
    return *cond_;
  }
  inline Exp&
  IfExp::cond_get()
  {
    return *cond_;
  }
  inline const Exp&
  IfExp::then_exp_get() const
  {
    return *then_exp_;
  }
  inline Exp&
  IfExp::then_exp_get()
  {
    return *then_exp_;
  }
  inline const Exp*
  IfExp::else_exp_get() const
  {
    return else_exp_;
  }
  inline Exp*
  IfExp::else_exp_get()
  {
    return else_exp_;
  }
} // namespace ast

