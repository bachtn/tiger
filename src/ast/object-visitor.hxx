/**
 ** \file ast/object-visitor.hxx
 ** \brief Implementation for ast/object-visitor.hh.
 */

#ifndef AST_OBJECT_VISITOR_HXX
# define AST_OBJECT_VISITOR_HXX

# include <misc/contract.hh>
# include <ast/all.hh>
# include <ast/object-visitor.hh>

namespace ast
{

  template <template <typename> class Const>
  GenObjectVisitor<Const>::GenObjectVisitor()
    : GenVisitor<Const>()
  {}

  template <template <typename> class Const>
  GenObjectVisitor<Const>::~GenObjectVisitor()
  {}


  /*-------------------------------.
  | Object-related visit methods.  |
  `-------------------------------*/

  template <template <typename> class Const>
  void
  GenObjectVisitor<Const>::operator()(const_t<ClassTy>& e)
  {
    e.super_get().accept(*this);
    e.decs_get().accept(*this);
  }

  template <template <typename> class Const>
  void
  GenObjectVisitor<Const>::operator()(const_t<MethodDecs>& e)
  {
    misc::for_each(e.decs_get(), *this);
  }

  template <template <typename> class Const>
  void
  GenObjectVisitor<Const>::operator()(const_t<MethodDec>& e)
  {
    this->accept(&(e.formals_get()));
    this->accept(e.result_get());
    this->accept(e.body_get());
  }

  template <template <typename> class Const>
  void
  GenObjectVisitor<Const>::operator()(const_t<MethodCallExp>& e)
  {
    misc::for_each(e.arg_list_get(), *this);
    e.object_get().accept(*this);
  }

  template <template <typename> class Const>
  void
  GenObjectVisitor<Const>::operator()(const_t<ObjectExp>& e)
  {
    e.type_name_get().accept(*this);
  }


} // namespace ast

#endif // !AST_OBJECT_VISITOR_HXX
