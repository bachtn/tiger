/**
 ** \file ast/if-exp.cc
 ** \brief Implementation of ast::IfExp.
 */

#include <ast/visitor.hh>
#include <ast/if-exp.hh>

namespace ast
{
  IfExp::IfExp(const Location& location, Exp* cond, Exp* then_exp,
               Exp* else_exp)
  : Exp(location)
  , cond_(cond)
  , then_exp_(then_exp)
  , else_exp_(else_exp)
  {}

  IfExp::IfExp(const Location& location, Exp* cond, Exp* then_exp)
  : Exp(location)
  , cond_(cond)
  , then_exp_(then_exp)
  , else_exp_(nullptr)
  {}


  IfExp::~IfExp()
  {
    delete cond_;
    delete then_exp_;
    delete else_exp_;
  }

  void
  IfExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void
  IfExp::accept(Visitor& v)
  {
    v(*this);
  }
} // namespace ast
