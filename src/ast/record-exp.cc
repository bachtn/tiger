/**
 ** \file ast/record-exp.cc
 ** \brief Implementation of ast::RecordExp.
 */

#include <misc/algorithm.hh>
#include <ast/visitor.hh>
#include <ast/record-exp.hh>

namespace ast
{

  RecordExp::RecordExp(const Location& location, NameTy* type_name,
                       fieldinits_type* ft)
    : Exp(location)
    , type_name_(type_name)
    , ft_(ft)
  {}

  RecordExp::~RecordExp()
  {
    delete type_name_;
    misc::deep_clear(*ft_);
    delete ft_;
  }

  void RecordExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void RecordExp::accept(Visitor& v)
  {
    v(*this);
  }

} // namespace ast

