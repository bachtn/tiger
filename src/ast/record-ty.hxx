/**
 ** \file ast/record-ty.hxx
 ** \brief Inline methods of ast::RecordTy.
 */

#pragma once

#include <ast/record-ty.hh>

namespace ast
{
  inline const fields_type&
  RecordTy::ft_get() const
  {
    return *ft_;
  }

  inline fields_type&
  RecordTy::ft_get()
  {
    return *ft_;
  }

} // namespace ast

