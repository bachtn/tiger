/**
 ** \file ast/record-exp.hh
 ** \brief Declaration of ast::RecordExp.
 */

#pragma once

#include <ast/exp.hh>
#include <ast/field-init.hh>
#include <ast/name-ty.hh>

namespace ast
{

  /// RecordExp.
  class RecordExp : public Exp
  {
  public:
    // Construct a RecordExp node
    RecordExp(const Location& location, NameTy* type_name,
              fieldinits_type* ft);
    // Destroy a RecordExp node
    ~RecordExp();

    // Accept a const visitor v
    virtual void accept(ConstVisitor& v) const override;
    // Accept a non-const visitor v
    virtual void accept(Visitor& v) override;

    //Return type name of the record (const)
    const NameTy& type_name_get() const;
    //Return type name of the record (non-const)
    NameTy& type_name_get();
    //Return the fieldinits_vect (const)
    const fieldinits_type& ft_get() const;
    //Return the fieldinits_vect (non-const)
    fieldinits_type& ft_get();

  protected:
    NameTy* type_name_;
    fieldinits_type* ft_;

  };

} // namespace ast

#include <ast/record-exp.hxx>

