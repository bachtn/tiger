/**
 ** \file ast/let-exp.cc
 ** \brief Implementation of ast::LetExp.
 */

#include <ast/visitor.hh>
#include <ast/let-exp.hh>
#include <misc/algorithm.hh>

namespace ast
{
  LetExp::LetExp(const Location& location, DecsList* let, Exp* in)
  : Exp(location)
  , let_(let)
  , in_(in)
  {}

  LetExp::~LetExp()
  {
    if (let_)
      delete let_;
    if (in_)
      delete in_;
  }

  void
  LetExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void
  LetExp::accept(Visitor& v)
  {
    v(*this);
  }

} // namespace ast

