/**
 ** \file ast/assign-exp.hxx
 ** \brief Inline methods of ast::AssignExp.
 */

#pragma once

#include <ast/assign-exp.hh>

namespace ast
{

  inline const Var& AssignExp::var_get() const
  {
    return *var_;
  }

  inline Var& AssignExp::var_get()
  {
    return *var_;
  }

  inline const Exp& AssignExp::value_get() const
  {
    return *value_;
  }

  inline Exp& AssignExp::value_get()
  {
    return *value_;
  }
} // namespace ast

