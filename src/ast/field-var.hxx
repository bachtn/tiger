/**
 ** \file ast/field-var.hxx
 ** \brief Inline methods of ast::FieldVar.
 */

#pragma once

#include <ast/field-var.hh>

namespace ast
{
  inline const Var&
  FieldVar::var_get() const
  {
    return *var_;
  }

  inline Var&
  FieldVar::var_get()
  {
    return *var_;
  }

  inline const misc::symbol&
  FieldVar::id_name_get() const
  {
    return id_name_;
  }

  inline misc::symbol&
  FieldVar::id_name_get()
  {
    return id_name_;
  }

} // namespace ast

