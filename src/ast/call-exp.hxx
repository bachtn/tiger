/**
 ** \file ast/call-exp.hxx
 ** \brief Inline methods of ast::CallExp.
 */

#pragma once

#include <ast/call-exp.hh>

namespace ast
{

  inline const misc::symbol& CallExp::name_get() const
  {
    return fn_name_;
  }

  inline misc::symbol& CallExp::name_get()
  {
    return fn_name_;
  }

  inline void CallExp::name_set(const misc::symbol& name)
  {
    fn_name_ = name;
  }

  inline const exps_type& CallExp::arg_list_get() const
  {
    return *arg_list_;
  }

  inline exps_type& CallExp::arg_list_get()
  {
    return *arg_list_;
  }

  inline const ast::FunctionDec*
  CallExp::def_get() const
  {
    return def_;
  }

  inline ast::FunctionDec*
  CallExp::def_get()
  {
    return def_;
  }

  inline void
  CallExp::def_set(ast::FunctionDec* def)
  {
    def_ = def;
  }
} // namespace ast

