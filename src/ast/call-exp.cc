/**
 ** \file ast/call-exp.cc
 ** \brief Implementation of ast::CallExp.
 */

#include <misc/algorithm.hh>
#include <ast/visitor.hh>
#include <ast/call-exp.hh>

namespace ast
{

  CallExp::CallExp(const Location& location, const misc::symbol& fn_name,
                   exps_type* arg_list)
    : Exp(location)
    , fn_name_(fn_name)
    , arg_list_(arg_list)
  {}

  CallExp::~CallExp()
  {
    misc::deep_clear(*arg_list_);
    delete arg_list_;
  }

  void CallExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void CallExp::accept(Visitor& v)
  {
    v(*this);
  }

} // namespace ast

