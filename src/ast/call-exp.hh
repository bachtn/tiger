/**
 ** \file ast/call-exp.hh
 ** \brief Declaration of ast::CallExp.
 */

#pragma once

#include <ast/exp.hh>
#include <ast/function-dec.hh>
#include <misc/symbol.hh>

namespace ast
{

  /// CallExp.
  class CallExp : public Exp
  {
  public:
    CallExp(const Location& location, const misc::symbol& fn_name,
            exps_type* arg_list);
    virtual ~CallExp();
    virtual void accept(ConstVisitor& v) const override;
    virtual void accept(Visitor& v) override;
    const misc::symbol& name_get() const;
    misc::symbol& name_get();
    void name_set(const misc::symbol&);
    const exps_type& arg_list_get() const;
    exps_type& arg_list_get();
    const ast::FunctionDec* def_get() const;
    ast::FunctionDec* def_get();
    void def_set(ast::FunctionDec* def);

  protected:
    misc::symbol fn_name_;
    exps_type* arg_list_;
    ast::FunctionDec* def_ = nullptr;
  };

} // namespace ast

#include <ast/call-exp.hxx>

