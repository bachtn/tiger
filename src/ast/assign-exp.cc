/**
 ** \file ast/assign-exp.cc
 ** \brief Implementation of ast::AssignExp.
 */

#include <ast/visitor.hh>
#include <ast/assign-exp.hh>

namespace ast
{

  AssignExp::AssignExp(const Location& location, Var* var, Exp* value)
    : Exp(location)
    , var_(var)
    , value_(value)
  {}

  AssignExp::~AssignExp()
  {
    delete var_;
    delete value_;
  }

  void AssignExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void AssignExp::accept(Visitor& v)
  {
    v(*this);
  }

} // namespace ast

