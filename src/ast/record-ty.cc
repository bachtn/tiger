/**
 ** \file ast/record-ty.cc
 ** \brief Implementation of ast::RecordTy.
 */

#include <misc/algorithm.hh>
#include <ast/visitor.hh>
#include <ast/record-ty.hh>

namespace ast
{
  RecordTy::RecordTy(const Location& location, fields_type* ft)
    : Ty(location)
    , ft_(ft)
  {}

  RecordTy::~RecordTy()
  {
      misc::deep_clear(*ft_);
      delete ft_;
  }

  void
  RecordTy::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void
  RecordTy::accept(Visitor& v)
  {
    v(*this);
  }

} // namespace ast

