/**
 ** \file ast/array-exp.hh
 ** \brief Declaration of ast::ArrayExp.
 */

#pragma once

#include <ast/exp.hh>
#include <ast/name-ty.hh>

namespace ast
{

  /// ArrayExp.
  class ArrayExp : public Exp
  {
  public:
    // Construct a ArrayExp node.
    ArrayExp(const Location& location, NameTy* type_name, Exp* size,
      Exp* init);
    // Destroy a ArrayExp node
    virtual ~ArrayExp();

    // Accept a const visitor v.
    virtual void accept(ConstVisitor& v) const override;
    // Accept a non-const visitor v.
    virtual void accept(Visitor& v) override;

    // Return type name of the declared array (const)
    const NameTy& type_name_get() const;
    // Return type name of the declared array (non-const)
    NameTy& type_name_get();
    // Return the size of the array (const)
    const Exp& size_get() const;
    // Return the size of the array (non-const)
    Exp& size_get();
    // Return the initial value (expression) assigned to the array (const)
    const Exp& init_get() const;
    // Return the initial value (expression) assigned to the array (non-const)
    Exp& init_get();

  protected:
    // Type of the declared array.
    NameTy* type_name_;
    // Size of the array.
    Exp* size_;
    // Initial value (expression) assigned to the array.
    Exp* init_;
  };

} // namespace ast

#include <ast/array-exp.hxx>

