/**
 ** \file ast/field-var.hh
 ** \brief Declaration of ast::FieldVar.
 */

#pragma once

#include <ast/var.hh>
#include <misc/symbol.hh>

namespace ast
{

  /// FieldVar.
  class FieldVar : public Var
  {
    public:
      FieldVar(const Location& location, Var* var, const misc::symbol& id_name);
      virtual ~FieldVar();

    public:
      virtual void accept(ConstVisitor& v) const override;
      virtual void accept(Visitor& v) override;

    public:
      const Var& var_get() const;
      Var& var_get();
      const misc::symbol& id_name_get() const;
      misc::symbol& id_name_get();

    protected:
      Var* var_;
      misc::symbol id_name_;
  };

} // namespace ast

#include <ast/field-var.hxx>

