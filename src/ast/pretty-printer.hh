/**
 ** \file ast/pretty-printer.hh
 ** \brief Declaration of ast::PrettyPrinter.
 */

#ifndef AST_PRETTY_PRINTER_HH
# define AST_PRETTY_PRINTER_HH

# include <ast/default-visitor.hh>
# include <ast/object-visitor.hh>

namespace ast
{

  /// Visit an Ast and print the content of each node.
  class PrettyPrinter
    : virtual public DefaultConstVisitor
    , virtual public ObjectConstVisitor
  {
  public:
    using super_type = DefaultConstVisitor;
    // Import overloaded virtual functions.
    using super_type::operator();

    // Used with the option color
    template <typename Colorified>
    std::string color (bool colored, int color_choice, Colorified color_this);

    /// Build to print on \a ostr.
    PrettyPrinter(std::ostream& ostr);

    /// Visit methods.
    /// \{

    /* VAR */
    virtual void operator()(const SimpleVar& e);
    virtual void operator()(const FieldVar& e);
    virtual void operator()(const SubscriptVar& e);
    virtual void operator()(const CastVar& e);
    /* EXP*/
    virtual void operator()(const ArrayExp& e);
    virtual void operator()(const AssignExp& e);
    virtual void operator()(const BreakExp& e);
    virtual void operator()(const CallExp& e);
    virtual void operator()(const MethodCallExp& e);
    /**/
    virtual void operator()(const CastExp& e);
    /**/
    virtual void operator()(const ForExp& e);
    virtual void operator()(const IfExp& e);
    virtual void operator()(const IntExp& e);
    virtual void operator()(const LetExp& e);
    virtual void operator()(const NilExp&);
    virtual void operator()(const ObjectExp& e);
    virtual void operator()(const OpExp& e);
    virtual void operator()(const RecordExp& e);
    virtual void operator()(const SeqExp& e);
    virtual void operator()(const StringExp& e);
    virtual void operator()(const WhileExp& e);

    /* TY */
    virtual void operator()(const ArrayTy& e);
    virtual void operator()(const ClassTy& e);
    virtual void operator()(const NameTy& e);
    virtual void operator()(const RecordTy& e);

    /* DEC */
    virtual void operator()(const Dec& e);
    virtual void operator()(const FunctionDec& e);
    virtual void operator()(const MethodDec& e);
    virtual void operator()(const TypeDec& e);
    virtual void operator()(const VarDec& e);

    virtual void operator()(const DecsList& e);
    virtual void operator()(const Field& e);
    virtual void operator()(const FieldInit& e);
    /// \}

  private:
    // Factor pretty-printing of RecordExp and RecordTy.
    template <typename RecordClass>
    void print_record(const RecordClass& e);

    template <typename FunctionClass>
    void print_function(const FunctionClass& e, std::string str);

    template <typename Class>
    void print_class(const Class& e);

  protected:
    /// The stream to print on.
    std::ostream& ostr_;
  };


} // namespace ast

#endif // !AST_PRETTY_PRINTER_HH
