/**
 ** \file ast/method-call-exp.cc
 ** \brief Implementation of ast::MethodCallExp.
 */

#include <ast/visitor.hh>
#include <ast/method-call-exp.hh>

namespace ast
{
  MethodCallExp::MethodCallExp(const Location& location, Var* object,
                               const misc::symbol& fn_name,
                               exps_type* arg_list)
    : CallExp(location, fn_name, arg_list)
    , object_(object)
  {}

  MethodCallExp::~MethodCallExp()
  {
    delete object_;
  }

  void MethodCallExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void MethodCallExp::accept(Visitor& v)
  {
    v(*this);
  }

} // namespace ast

