/**
 ** \file ast/assign-exp.hh
 ** \brief Declaration of ast::AssignExp.
 */

#pragma once

#include <ast/exp.hh>
#include <ast/var.hh>

namespace ast
{

  /// AssignExp.
  class AssignExp : public Exp
  {
  public:
    AssignExp(const Location& location, Var* var, Exp* value);
    virtual ~AssignExp();
    virtual void accept(ConstVisitor& v) const override;
    virtual void accept(Visitor& v) override;
    const Var& var_get() const;
    Var& var_get();
    const Exp& value_get() const;
    Exp& value_get();
  protected:
    Var* var_;
    Exp* value_;
  };

} // namespace ast

#include <ast/assign-exp.hxx>

