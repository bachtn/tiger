/**
 ** \file ast/op-exp.cc
 ** \brief Implementation of ast::OpExp.
 */

#include <ast/visitor.hh>
#include <ast/op-exp.hh>

namespace ast
{

  OpExp::OpExp(const Location& location, Exp* left, OpExp::Oper oper,
               Exp* right)
    : Exp(location)
    , left_(left)
    , oper_(oper)
    , right_(right)
  {}

  OpExp::~OpExp()
  {
    delete left_;
    delete right_;
  }

  void
  OpExp::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void
  OpExp::accept(Visitor& v)
  {
    v(*this);
  }

} // namespace ast


  std::string str(ast::OpExp::Oper oper)
  {
    switch (oper)
    {
    case ast::OpExp::add:
      return "+";
    case ast::OpExp::sub:
      return "-";
    case ast::OpExp::mul:
      return "*";
    case ast::OpExp::div:
      return "/";
    case ast::OpExp::eq:
      return "=";
    case ast::OpExp::ne:
      return "<>";
    case ast::OpExp::lt:
      return "<";
    case ast::OpExp::le:
      return "<=";
    case ast::OpExp::gt:
      return ">";
    case ast::OpExp::ge:
      return ">=";
    default:
      return "error in opexp.cc";
    }
  }

