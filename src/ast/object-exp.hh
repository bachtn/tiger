/**
 ** \file ast/object-exp.hh
 ** \brief Declaration of ast::ObjectExp.
 */

#pragma once

#include <ast/exp.hh>
#include <ast/name-ty.hh>

namespace ast
{

  /// ObjectExp.
  class ObjectExp : public Exp
  {
    public:
      ObjectExp(const Location&_location, NameTy* type_name);
      virtual ~ObjectExp();

    public:
      virtual void accept(ConstVisitor& v) const override;
      virtual void accept(Visitor& v) override;

    public:
      const NameTy& type_name_get() const;
      NameTy& type_name_get();

    private:
      NameTy* type_name_;
  };

} // namespace ast

#include <ast/object-exp.hxx>

