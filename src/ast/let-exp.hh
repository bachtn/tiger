/**
 ** \file ast/let-exp.hh
 ** \brief Declaration of ast::LetExp.
 */

#pragma once

#include <ast/decs-list.hh>
#include <ast/exp.hh>
# include <misc/contract.hh>

namespace ast
{

  /// LetExp.
  class LetExp : public Exp
  {
    public:
      LetExp(const Location& location, DecsList* let, Exp* in);
      virtual ~LetExp();

    public:
      virtual void accept(ConstVisitor& v) const override;
      virtual void accept(Visitor& v) override;

    public:
      const DecsList& let_get() const;
      DecsList& let_get();
      const Exp& in_get() const;
      Exp& in_get();

    protected:
      DecsList* let_;
      Exp* in_;
  };

} // namespace ast

#include <ast/let-exp.hxx>

