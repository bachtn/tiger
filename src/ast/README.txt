* README

Tiger Abstract Syntax Tree nodes with their principal members.
Incomplete classes are tagged with a `*'.

/Ast/               (Location location)
  /Dec/             (symbol name)
    FunctionDec     (VarDecs formals, NameTy result, Exp body)
      MethodDec     ()
    TypeDec         (Ty ty)
    VarDec          (NameTy type_name, Exp init)

  /Exp/             ()
    /Var/           ()                                                   lvalue
      CastVar       (Var var, Ty ty)                                            _cast ( lvalue , ty )
*     FieldVar      (Var var, symbol id_name)                                   lvalue . id
      SimpleVar     (symbol name)                                               id
      SubscriptVar  (Var var, Exp index)                                        lvalue [ exp ]

                                                                         exp
*   ArrayExp        (NameTy type_name, Exp size, Exp init)                      type-id [ exp ] OF exp                    tab[10] of 0
*   AssignExp       (Var var, Exp value)                                        lvalue := exp                             a := 2
*   BreakExp        ()                                                          BREAK
*   CallExp         (symbol fn_name, exps_type arg_list)                        id ([ exp { , exp }])                     fact(5, 10)
*     MethodCallExp (Var object, symbol fn_name, exps_type arg_list)            lvalue . id ( [exp { , exp}] )            obj.fact(5, 10)
    CastExp         (Exp exp, Ty ty)
    ForExp          (VarDec vardec, Exp hi, Exp body)                           FOR id := exp TO exp DO exp               for i := 5 to 8 do fact(5)
*   IfExp           (Exp cond, Exp then_exp, [Exp else_exp])                    IF exp THEN exp [ELSE exp]                if a then b else c
    IntExp          (int value)                                                 integer                                   10
*   LetExp          (DecsList let, exps_type in)                                LET decs IN exps END                      let var a := 5 in print(a) end
    NilExp          ()                                                          nil                                       nil
*   ObjectExp       (NameTy type_name)                                          NEW type-id                               new object
    OpExp           (Exp left, Oper oper, Exp right)                            +-*/..                                    a + b
*   RecordExp       (NameTy type_name, fieldinits_type fieldinits_vect)         type-id { [id = exp {, id = exp}] }       my_record {value1 = 1, value2 = b}
*   SeqExp          (exps_type exps)                                            ( exps )                                  ( a := 1; b :=2; print(a + b) )
*   StringExp       (std::string str)                                           string                                    "hello\n"
    WhileExp        (Exp test, Exp body)                                        WHILE exp DO exp                          while true do print("hello")

  /Ty/              ()                                                    ty
    ArrayTy         (NameTy base_type)                                          ARRAY OF type-id                          array of int
    ClassTy         (NameTy super, DecsList decs)                               CLASS [EXTENDS type-id] {classfields}     class extends Object
    NameTy          (symbol name)
*   RecordTy        (fields_type ft)                                                                                      { index : int, value : string }

  DecsList          (decs_type decs)

  Field             (symbol name, NameTy type_name)     id : type-id

  FieldInit         (symbol name, Exp init)             id  = exp


Some of these classes also inherit from other classes.

/Escapable/
  VarDec            (NameTy type_name, Exp init)

/Typable/
  /Dec/             (symbol name)
  /Exp/             ()
  /Ty/              ()

/TypeConstructor/
  /Ty/              ()
  FunctionDec       (VarDecs formals, NameTy result, Exp body)
  NilExp            ()
  TypeDec           (Ty ty)

