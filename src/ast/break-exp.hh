/**
 ** \file ast/break-exp.hh
 ** \brief Declaration of ast::BreakExp.
 */

#pragma once

#include <ast/exp.hh>

namespace ast
{

  /// BreakExp.
  class BreakExp : public Exp
  {
  public:
    BreakExp(const Location& location);
    virtual ~BreakExp();
    virtual void accept(ConstVisitor& v) const override;
    virtual void accept(Visitor& v) override;
    const Exp* def_get() const;
    Exp* def_get();
    void def_set(Exp* def);
  protected:
    Exp* def_ = nullptr;
  };

} // namespace ast

#include <ast/break-exp.hxx>

