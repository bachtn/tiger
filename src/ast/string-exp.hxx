/**
 ** \file ast/string-exp.hxx
 ** \brief Inline methods of ast::StringExp.
 */

#pragma once

#include <ast/string-exp.hh>

namespace ast
{

  inline const std::string& StringExp::str_get() const
  {
    return str_;
  }

  inline std::string& StringExp::str_get()
  {
    return str_;
  }
} // namespace ast

