/**
 ** \file ast/function-dec.cc
 ** \brief Implementation of ast::FunctionDec.
 */

#include <ast/visitor.hh>
#include <ast/function-dec.hh>

namespace ast
{

  FunctionDec::FunctionDec(const Location& location,
                           const misc::symbol& name, VarDecs* formals,
                           NameTy* result, Exp* body)
    : Dec(location, name)
    , TypeConstructor()
    , formals_(formals)
    , result_(result)
    , body_(body)
  {}

  FunctionDec::~FunctionDec()
  {
    if (formals_)
      delete formals_;
    if (result_)
      delete result_;
    if (body_)
      delete body_;
  }

  void
  FunctionDec::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void
  FunctionDec::accept(Visitor& v)
  {
    v(*this);
  }

} // namespace ast

