/**
 ** \file ast/record-ty.hh
 ** \brief Declaration of ast::RecordTy.
 */

#pragma once

#include <ast/field.hh>
#include <ast/ty.hh>

namespace ast
{

  /// RecordTy.
  class RecordTy : public Ty
  {
    public:
      RecordTy(const Location& location, fields_type* ft);
      virtual ~RecordTy();

    public:
      virtual void accept(ConstVisitor& v) const override;
      virtual void accept(Visitor& v) override;

    public:
      const fields_type& ft_get() const;
      fields_type& ft_get();

    protected:
      fields_type* ft_;

  };

} // namespace ast

#include <ast/record-ty.hxx>

