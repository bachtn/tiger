/**
 ** \file ast/let-exp.hxx
 ** \brief Inline methods of ast::LetExp.
 */

#pragma once

#include <ast/let-exp.hh>

namespace ast
{
  inline const DecsList&
  LetExp::let_get() const
  {
    return *let_;
  }
  inline DecsList&
  LetExp::let_get()
  {
    return *let_;
  }
  inline const Exp&
  LetExp::in_get() const
  {
    return *in_;
  }
  inline Exp&
  LetExp::in_get()
  {
    return *in_;
  }
} // namespace ast

