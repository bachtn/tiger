/**
 ** \file bind/libbind.hh
 ** \brief Interface of the bind module.
 */

#pragma once

# include <misc/error.hh>
# include <utility>
# include <ast/ast.hh>
# include <bind/renamer.hh>

namespace bind
{
  misc::error bind(ast::Ast& n);
  void rename(ast::Ast& n);

} // namespace bind


  // FIXME: Some code was deleted here.
