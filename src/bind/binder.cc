/**
 ** \file bind/binder.cc
 ** \brief Implementation for bind/binder.hh.
 */

#include <bind/binder.hh>
#include <misc/contract.hh>
#include <ast/all.hh>


namespace bind
{
  //constructor
  Binder::Binder()
  {
    scop_fun = misc::scoped_map<misc::symbol, ast::FunctionDec*>();
    scop_var = misc::scoped_map<misc::symbol, ast::VarDec*>();
    scop_ty = misc::scoped_map<misc::symbol, ast::TypeDec*>();
    scope_begin();
  }

  /*-----------------.
  | Error handling.  |
  `-----------------*/

 void Binder::error(const ast::Ast& loc, const std::string& msg)
  {
    error_ << misc::error::bind
          << loc.location_get()
          << ": " << msg
          << std::endl;
  }

  /// The error handler.
  const misc::error&
  Binder::error_get() const
  {
    return error_;
  }

  /*----------------.
  | Symboltables.  |
  `----------------*/


  void
  Binder::scope_begin()
  {
    scop_fun.scope_begin();
    scop_var.scope_begin();
    scop_ty.scope_begin();
  }

  void
  Binder::scope_end()
  {
    scop_fun.scope_end();
    scop_var.scope_end();
    scop_ty.scope_end();
  }

  /*---------.
  | Visits.  |
  `---------*/


  void
  Binder::operator()(ast::LetExp& e)
  {
    scope_begin();
    ast::Exp* loop_ptr = loop_ptr_;
    loop_ptr_ = nullptr;
    bool linked = linked_;
    linked_ = false;
    super_type::operator()(e.let_get());
    loop_ptr_ = loop_ptr;
    linked_ = linked;
    super_type::operator()(e.in_get());
    scope_end();
  }

  void
  Binder::operator()(ast::SimpleVar& e)
  {
    auto tmp = scop_var.get(e.name_get());
    if (!tmp && e.name_get() != "self")
      undeclared("variable", e);
    def_default(e, tmp);
  }

  void
  Binder::operator()(ast::CallExp& e)
  {
    auto tmp = scop_fun.get(e.name_get());
    if (!tmp)
      undeclared("function", e);
    def_default(e, tmp);
    super_type::operator()(e);
  }

  void
  Binder::operator()(ast::NameTy& e)
  {
    auto tmp = scop_ty.get(e.name_get());
    auto name = e.name_get();
    if (!tmp && name != "int" && name != "string" && name != "Object")
      undeclared("type", e);
    else
    {
      def_default(e, tmp);
      super_type::operator()(e);
    }
  }

  void
  Binder::operator()(ast::ForExp& e)
  {
    scope_begin();
    ast::Exp* loop_ptr = loop_ptr_;
    loop_ptr_ = nullptr;
    bool linked = linked_;
    linked_ = false;
    super_type::operator()(e.hi_get());
    //operator()(e.vardec_get());
    //
    operator()(e.vardec_get().init_get());
    scop_var.put(e.vardec_get().name_get(), &(e.vardec_get()));
    //
    loop_ptr_ = &e;
    super_type::operator()(e.body_get());
    loop_ptr_ = loop_ptr;
    linked_ = linked;
    scope_end();
  }

  void
  Binder::operator()(ast::WhileExp& e)
  {
    ast::Exp* loop_ptr = loop_ptr_;
    loop_ptr_ = nullptr;
    bool linked = linked_;
    linked_ = false;
    super_type::operator()(e.test_get());
    loop_ptr_ = &e;
    super_type::operator()(e.body_get());
    loop_ptr_ = loop_ptr;
    linked_ = linked;
  }

  void
  Binder::operator()(ast::IfExp& e)
  {
    linked_ = false;
    super_type::operator()(e.cond_get());
    super_type::operator()(e.then_exp_get());
    linked_ = false;
    if (e.else_exp_get())
      super_type::operator()(e.else_exp_get());
    linked_ = false;
  }

  void
  Binder::operator()(ast::BreakExp& e)
  {

    if (loop_ptr_ != nullptr && !linked_)
    {
      e.def_set(loop_ptr_);
      linked_ = true;
    }
    else
    {
      std::stringstream ss;
      ss << ": `break` outside any loop";
      error(e, ss.str());
    }
  }

  void
  Binder::operator()(ast::VarDec& e)
  {
    scop_var.put(e.name_get(), &e);
    super_type::operator()(e);
  }

  /*-------------------.
  | Visiting VarDecs.  |
  `-------------------*/

  void
  Binder::operator()(ast::VarDecs& e)
  {
    decs_visit(e);
  }

  /*------------------------.
  | Visiting FunctionDecs.  |
  `------------------------*/
  void
  Binder::operator()(ast::FunctionDecs& e)
  {
    decs_visit(e);
  }


  /*--------------------.
  | Visiting TypeDecs.  |
  `--------------------*/
  void
  Binder::operator()(ast::TypeDecs& e)
  {
    decs_visit(e);
  }
} // namespace bind
