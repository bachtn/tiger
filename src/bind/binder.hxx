/**
 ** \file bind/binder.hxx
 ** \brief Inline methods of bind::Binder.
 **/

#pragma once

#include <bind/binder.hh>

namespace bind
{

  /*-----------------.
  | Error handling.  |
  `-----------------*/
  template <typename T>
  void Binder::undeclared(const std::string& k, const T& e)
  {
      std::stringstream ss;
      ss << "undeclared " << k << ": " << e.name_get();
      error(e, ss.str());
  }

  template <typename T>
  void Binder::redefinition(const T& e1, const T& e2)
  {
      std::stringstream ss;
      ss << ": redefinition: " << e2.name_get()
         << "\n" << e1.location_get() << ": first definition";
      error(e2, ss.str());
  }

  /*-------------------.
  | Definition sites.  |
  `-------------------*/

  template<typename NodeType, typename DefType>
  void Binder::def_default(NodeType& e, DefType* def)
  {
    if (!e.def_get())
      e.def_set(def);
  }

  /*------------------.
  | Visiting /Decs/.  |
  `------------------*/

  template <class D>
  void
  Binder::decs_visit(ast::AnyDecs<D>& e)
  {
    std::map<misc::symbol, D*> map;
    for (auto it = e.decs_get().begin(); it != e.decs_get().end(); it++)
    {
       auto dec_name = (**it).name_get();
       auto it_tmp = map.find(dec_name);
       if (it_tmp != map.end())
       {
         redefinition(*it_tmp->second, **it);
       }
       map.insert(std::make_pair(dec_name, *it));
       visit_dec_header(**it);
    }
    for (auto it = e.decs_get().begin(); it != e.decs_get().end(); it++)
       Binder::visit_dec_body(**it);
  }

template <>
 inline void
  Binder::visit_dec_header<ast::VarDec>(ast::VarDec& e)
  {
    super_type::operator()(e);
    scop_var.put(e.name_get(), &e);
  }

  template <>
  inline void
  Binder::visit_dec_body<ast::VarDec>(ast::VarDec&)
  {}

 template <>
  inline void
  Binder::visit_dec_header<ast::FunctionDec>(ast::FunctionDec& e)
  {
    scop_fun.put(e.name_get(), &e);
  }

  template <>
inline  void
  Binder::visit_dec_body<ast::FunctionDec>(ast::FunctionDec& e)
  {
    scope_begin();
    super_type::operator()(e);
    scope_end();
  }
template <>
  inline void
  Binder::visit_dec_header<ast::TypeDec>(ast::TypeDec& e)
  {
    scop_ty.put(e.name_get(), &e);
  }

  template <>
inline  void
  Binder::visit_dec_body<ast::TypeDec>(ast::TypeDec& e)
  {
    super_type::operator()(e);
  }
} // namespace bind
