/**
 ** \file bind/renamer.cc
 ** \brief Implementation of bind::Renamer.
 */

#include <bind/renamer.hh>

namespace bind
{
  using namespace ast;

  Renamer::Renamer()
    : super_type(), new_names_()
  {}

  //WIP FIXME: Some code was deleted here.
  void Renamer::operator()(ast::FunctionDec& e)
  {
    visit(e, &e);
  }

  void Renamer::operator()(ast::TypeDec& e)
  {
    visit(e, &e);
  }

  void Renamer::operator()(ast::VarDec& e)
  {
    visit(e, &e);
  }

  void Renamer::operator()(ast::CallExp& e)
  {
    visit(e, e.def_get());
  }

  void Renamer::operator()(ast::NameTy& e)
  {
    visit(e, e.def_get());
  }

  void Renamer::operator()(ast::SimpleVar& e)
  {
    visit(e, e.def_get());
  }


} // namespace bind
