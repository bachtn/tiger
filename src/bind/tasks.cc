/**
 ** \file bind/tasks.cc
 ** \brief Bind module tasks implementation.
 */

#include <misc/contract.hh>
#include <misc/error.hh>
#include <ast/tasks.hh>
#include <ast/libast.hh>
#include <bind/libbind.hh>
#include <common.hh>
#define DEFINE_TASKS 1
#include <bind/tasks.hh>
#undef DEFINE_TASKS


namespace bind
{
  namespace tasks
  {
    void bind_compute()
    {
        misc::error result = bind::bind(*(ast::tasks::the_program));
        task_error << result;
        task_error.exit_on_error();
    }

    void bind_display()
    {
       ast::bindings_display(std::cout) = true;
    }

    void rename_compute()
    {
        bind::rename(*(ast::tasks::the_program));
    }
  } // namespace tasks

} // namespace bind

  // FIXME: Some code was deleted here.
