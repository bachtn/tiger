/**
 ** \file bind/libbind.cc
 ** \brief Define exported bind functions.
 */

#include <bind/libbind.hh>
#include <bind/binder.hh>
#include <misc/symbol.hh>
#include <misc/file-library.hh>
#include <misc/contract.hh>
#include <ast/all.hh>
#include <ast/decs.hh>
#include <ast/decs-list.hh>

namespace bind
{
  misc::error bind(ast::Ast& n)
  {
      Binder b;
      b(n);
      return b.error_get();
  }

  void rename(ast::Ast& n)
  {
    Renamer r;
    r(n);
  }

} // namespace bind
  
  // FIXME: Some code was deleted here.
