/**
 ** \file bind/tasks.hh
 ** \brief Bind module related tasks.
 */
#pragma once

# include <task/libtask.hh>
# include <bind/libbind.hh>

namespace bind
{
  namespace tasks
  {
    TASK_GROUP("3. Bind");

   /* Compute binding */ 
    TASK_DECLARE("b|bindings-compute", "bind the identifiers",  bind_compute,
        "parse");
    
    /* Default computation without objects nor overloading */
    // disjonctive task make sure that dependence have be done
    /*TASK_DECLARE("bound",
    "default the computation of bindings to Tiger" +
    " (without objects nor overloading)", bound_compute, "parse");*/
    DISJUNCTIVE_TASK_DECLARE("bound",
    "default the computation of bindings to Tiger (without objects nor overloading)"
    , "bindings-compute object-bindings-compute");

    /* Display the declaration node address next to any symbol with
       pretty-printer */ 
    TASK_DECLARE("B|bindings-display", "enable bindings display in the AST",
                  bind_display, "parse");

    /* Rename identifiers to unique names */
    TASK_DECLARE("rename", "rename identifiers to unique names", rename_compute,
    "bindings-compute");
  } // name

} // namespace bind

  // FIXME: Some code was deleted here.
