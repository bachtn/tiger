/**
 ** \file bind/renamer.hxx
 ** \brief Template methods of bind::Renamer.
 */

#pragma once

#include <bind/renamer.hh>

namespace bind
{
  //WIP FIXME: Some code was deleted here.

template <>
  inline misc::symbol Renamer::new_name_compute<ast::FunctionDec>(const
  ast::FunctionDec& e)
  {
    auto name = e.name_get();
    if (name == "_main" || !e.body_get())
      return name;
    else
      {
        misc::symbol res = misc::symbol::fresh(e.name_get());
        new_names_.insert(std::pair<const ast::FunctionDec*, misc::symbol>(&e, res));
        return res;
      }
  }

  template <typename Def>
  misc::symbol Renamer::new_name_compute(const Def& e)
  {
    auto name = e.name_get();

    misc::symbol res = misc::symbol::fresh(e.name_get());
      new_names_.insert(std::pair<const Def*, misc::symbol>(&e, res));
      return res;
  }

  template <typename Def>
  misc::symbol Renamer::new_name(const Def& e)
  {
    auto match = new_names_.find(&e);

    if (match != new_names_.end())
      return match->second;

    //new_names_.insert(std::pair<const Def*, misc::symbol>(&e, e.name_get()));
    return new_name_compute(e);
  }



  template <class E, class Def>
  void Renamer::visit(E& e, const Def* def)
  {
    if (def != nullptr)
      e.name_set(new_name(*def));
    super_type::operator()(e);
  }
} // namespace bind
