/**
 ** \file type/builtin-types.hh
 ** \brief The classes Void, Int, String.
 */
#pragma once

#include <type/type.hh>
#include <type/fwd.hh>
#include <misc/singleton.hh>

namespace type
{
  class String : public Type, public misc::Singleton<String>
  {
    public:
      void accept(ConstVisitor& v) const override;
      void accept(Visitor& v) override;
      bool compatible_with(const Type& other) const override;
  };

  class Int : public Type, public misc::Singleton<Int>
  {
    public:
      void accept(ConstVisitor& v) const override;
      void accept(Visitor& v) override;
      bool compatible_with(const Type& other) const override;
  };

  class Void : public Type, public misc::Singleton<Void>
  {
    public:
      void accept(ConstVisitor& v) const override;
      void accept(Visitor& v) override;
      bool compatible_with(const Type& other) const override;
  };
  // FIXME: Some code was deleted here (Other types : String, Int, Void).

} // namespace type
