/**
 ** \file type/method.cc
 ** \brief Implementation for type/method.hh.
 */

#include <iostream>
#include <type/method.hh>
#include <type/visitor.hh>

namespace type
{

  Method::Method(const misc::symbol& name, const Class* owner,
                 const Record* formals, const Type& result,
                 ast::MethodDec* def)
    : Function(formals, result)
    , name_(name)
    , owner_(owner)
    , def_(def)
  {}

  Method::~Method()
  {}

  void
  Method::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void
  Method::accept(Visitor& v)
  {
    v(*this);
  }
  
  bool Method::compatible_with(const Type& other) const
  {
    const auto child = dynamic_cast<const Method*>(&other);
    if (child && result_ == child->result_get()
      && *formals_ == child->formals_get()
      && name_ == child->name_get()
      && owner_ == child->owner_get())
      return true;
    return false;
  }
  // FIXME: Some code was deleted here.

} // namespace type
