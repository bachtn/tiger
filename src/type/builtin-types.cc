/**
 ** \file type/builtin-types.cc
 ** \brief Implementation for type/builtin-types.hh.
 */

#include <ostream>

#include <type/builtin-types.hh>
#include <type/visitor.hh>

namespace type
{
  /*String methods */
  
  void String::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void String::accept(Visitor& v)
  {
    v(*this);
  }

  bool String::compatible_with(const Type& other) const
  {
    return String::instance() == other;
  }

 /* Int methods */

  void Int::accept(ConstVisitor& v) const 
  {
    v(*this);
  }

  void Int::accept(Visitor& v)
  {
    v(*this);
  }

  bool Int::compatible_with(const Type& other) const
  {
    return Int::instance() == other;
  }

  /* Void methods */

  void Void::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void Void::accept(Visitor& v)
  {
    v(*this);
  }

  bool Void::compatible_with(const Type& other) const
  {
    return Void::instance() == other;
  }

  // FIXME: Some code was deleted here (Void, Int, and String).

} // namespace type
