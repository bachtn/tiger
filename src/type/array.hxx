/**
 ** \file type/array.hxx
 ** \brief Inline methods for type::Array.
 */
#pragma once

#include <type/array.hh>
#include <misc/contract.hh>

namespace type
{
  inline
  Array::Array(const Type* type)
  {
    type_ = type;
  }
  inline
  Array::~Array(){}

  // FIXME: Some code was deleted here (Ctor & Dtor).
} // namespace type
