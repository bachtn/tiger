/**
 ** \file type/class.cc
 ** \brief Implementation for type/class.hh.
 */

#include <iostream>
#include <ostream>

#include <misc/indent.hh>
#include <type/class.hh>
#include <type/visitor.hh>

namespace type
{
  Class::Class (const Class* super)
    : Type ()
    , id_ (fresh_id ())
    , super_ (super)
    , subclasses_ ()
  {}

  void
  Class::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void
  Class::accept(Visitor& v)
  {
    v(*this);
  }

  const Type*
  Class::attr_type(const misc::symbol& key) const
  {
    const Attribute* a = attr_find(key);
    if (a)
      return &(a->type_get());
    return nullptr;

  // FIXME: Some code was deleted here.
  }

  const Method*
  Class::meth_type(const misc::symbol& key) const
  {
    for (const Class* cur = this; cur; cur = cur->super_get())
    {
      const Method* meth = cur->owned_meth_find(key);
      if (meth)
        return meth;
    }
    return nullptr;
  // FIXME: Some code was deleted here.
  }
  
  const Class* Class::common_root(const Class& other) const
  {
    for (const Class* cur1 = this; cur1; cur1 = cur1->super_get())
    {
      for (const Class* cur2 = &other; cur2; cur2 = cur2->super_get())
      {
        if (cur1 == cur2)
          return cur1;
      }
    }
    return nullptr;
  }
  // FIXME: Some code was deleted here (Find common super class).
 // utiliser un set afin de vérifer le cas a->b; b->c; c->b 
  bool Class::sound() const
  {
    std::set<const Class*> cl = std::set<const Class*>();
    for (const Class* cur = this; cur; cur = cur->super_get())
    {
      if (cl.find(cur) != cl.end())
        return false;
      cl.insert(cur);
    }
    return true;
  }
  // FIXME: Some code was deleted here (Super class soundness test).
 
  // utiliser le dynamique cast pour savoir si un child peut un parent et donc
  // si other un parent
  bool Class::compatible_with(const Type& other) const
  {
    const auto child = dynamic_cast<const Class*>(&other);
    if (child)
      return true;
    const auto child2 = dynamic_cast<const Nil*>(&other);
    if (child2)
      return true;
    return *this == other;
  }
  // FIXME: Some code was deleted here (Special implementation of "compatible_with" for Class).

  const Class&
  Class::object_instance()
  {
    static Class obj = new Class();
    return obj;
  // FIXME: Some code was deleted here.
  }

  unsigned Class::fresh_id()
  {
    static unsigned counter_ = 0;
    return counter_++;
  }

} // namespace type
