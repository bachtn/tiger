/**
 ** \file type/array.hh
 ** \brief The class Array.
 */
#pragma once

#include <type/type.hh>
#include <type/fwd.hh>

namespace type
{

  /// Array types.
  class Array : public Type
  {
    public:
      Array(const Type* type);
      ~Array();
      void accept(ConstVisitor& v) const override;
      void accept(Visitor& v) override;
      const Type& type_get() const;
    private:
      const Type* type_;
  // FIXED: Some code was deleted here.
  };


} // namespace type

#include <type/array.hxx>
