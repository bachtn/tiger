/**
 ** \file type/array.cc
 ** \brief Implementation for type/array.hh.
 */

#include <ostream>

#include <type/array.hh>
#include <type/visitor.hh>

namespace type
{
  const Type& Array::type_get() const
  {
    return *type_;
  }

  void Array::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void Array::accept(Visitor& v)
  {
    v(*this);
  }


  // FIXME: Some code was deleted here.

} // namespace type
