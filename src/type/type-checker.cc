/*
 ** \file type/type-checker.cc
 ** \brief Implementation for type/type-checker.hh.
 */

#include <ast/all.hh>
#include <type/types.hh>
#include <type/type-checker.hh>

#include <memory>

namespace type
{

  namespace
  {
    // Try to cast the type into a nil type.
    // If it's not actually a `type::Nil`, return `nullptr`.
    // This allows us to write more clear code like:
    // if (auto nil = to_nil(e.type_get())
    // ...
    const Nil* to_nil(const Type& type)
    {
      return dynamic_cast<const Nil*>(&type.actual());
    }
  }

  TypeChecker::TypeChecker()
    : super_type()
      , error_()
  {}

  const Type*
    TypeChecker::type(ast::Typable& e)
    {
      // FIXED: Some code was deleted here.
      if (e.type_get() == nullptr)
        e.accept(*this);

      return e.type_get();
    }

  const Record*
    TypeChecker::type(const ast::fields_type& e)
    {
      auto res = new Record;
      // FIXED: Some code was deleted here.
      for (unsigned i = 0; i < e.size(); i++)
        res->field_add(e[i]->name_get(), *type(e[i]->type_name_get()));

      return res;
    }

  const Record*
    TypeChecker::type(const ast::VarDecs& e)
    {
      auto res = new Record;
      for (const auto& i : e.decs_get())
        res->field_add(i->name_get(), *type(*i));

      return res;
    }

  const misc::error&
    TypeChecker::error_get() const
    {
      return error_;
    }


  /*-----------------.
    | Error handling.  |
    `-----------------*/

  void
    TypeChecker::error(const ast::Ast& loc, const std::string& msg)
    {
      error_ << misc::error::type
        << loc.location_get() << ": " << msg << std::endl;
    }

  void
    TypeChecker::type_mismatch(const ast::Ast& loc,
        const std::string& exp1, const Type& type1,
        const std::string& exp2, const Type& type2)
    {
      error_ << misc::error::type
        << loc.location_get() << ": type mismatch" << misc::incendl
        << exp1 << " type: " << type1 << misc::iendl
        << exp2 << " type: " << type2 << misc::decendl;
    }

  void
    TypeChecker::check_types(const ast::Ast& loc,
        const std::string& exp1, const Type& type1,
        const std::string& exp2, const Type& type2)
    {
      // FIXED: Some code was deleted here.
      if (!type1.compatible_with(type2))
        type_mismatch(loc, exp1, type1, exp2, type2);
    }


  void
    TypeChecker::check_types(const ast::Ast& loc,
        const std::string& exp1, ast::Typable& type1,
        const std::string& exp2, ast::Typable& type2)
    {
      // Ensure evaluation order.
      type(type1);
      type(type2);
      // FIXED: Some code was deleted here (Check types).
      check_types(loc, exp1, *(type1.type_get()), exp2, *(type2.type_get()));
    }


  /*--------------------------.
    | The core of the visitor.  |
    `--------------------------*/

  /*-----------------.
    | Visiting /Var/.  |
    `-----------------*/

  void
    TypeChecker::operator()(ast::SimpleVar& e)
    {
      // FIXED: Some code was deleted here // Check the readonly stuff.
      type_default(e, e.def_get()->type_get());
    }

  // FIXED: Some code was deleted here.
  void
    TypeChecker::operator()(ast::CastVar& e)
    {
      // FIXED: Some code was deleted here.
      type_default(e, e.ty_get().type_get());
    }

  void
    TypeChecker::operator()(ast::FieldVar& e)
    {
      // type(e.var_get()))
      // serach for the champ id in the fieldVar -> when found he is already typed
      // type the filed var to the the type of the id
      // FIXME: Some code was deleted here.
      type(e.var_get());
      const auto ar = dynamic_cast<const
        Record*>(&e.var_get().type_get()->actual());
      if (ar == nullptr)
      {
        error(e.var_get(), "This node is not a record.");
        type_default(e, &(Void::instance()));
      }
      else
      {
        if (ar->field_index(e.id_name_get()) == -1) //field doesn't exist
        {
          error(e, "Invalid field");
          type_default(e, &(Void::instance()));
        }
        else
        {
          type_default(e, ar->field_type(e.id_name_get()));
        }
      }

    }

  void
    TypeChecker::operator()(ast::SubscriptVar& e)
    {
      // FIXED: Some code was deleted here.
      type(e.var_get());
      type(e.index_get());
      const auto ar = dynamic_cast<const
        Array*>(&e.var_get().type_get()->actual());
      if (ar == nullptr)
      {
        type_default(e, &(Void::instance()));
        error(e.var_get(), "This node is not an array");
      }
      else
      {
        check_type(e.index_get(), "index", Int::instance());
        type_default(e, &(ar->type_get()));
      }
    }

  /*-----------------.
    | Visiting /Exp/.  |
    `-----------------*/

  // Literals.
  void
    TypeChecker::operator()(ast::NilExp& e)
    {
      auto nil_ptr = std::make_unique<Nil>();
      type_default(e, nil_ptr.get());
      created_type_default(e, nil_ptr.release());
    }

  void
    TypeChecker::operator()(ast::IntExp& e)
    {
      // FIXED: Some code was deleted here.
      type_default(e, &(Int::instance()));
    }

  void
    TypeChecker::operator()(ast::ArrayExp& e)
    {
      type(e.type_name_get());
      type(e.size_get());
      type(e.init_get());
      const auto ar = dynamic_cast<const
        Array*>(&e.type_name_get().type_get()->actual());
      if (ar == nullptr)
        type_mismatch(e, "name", *(e.type_name_get().type_get()), "init",
            *(e.init_get().type_get()));
      check_type(e.size_get(), "size", Int::instance());
      check_types(e, "type name", ar->type_get(), "init value",
          *e.init_get().type_get());
      type_default(e, e.type_name_get().type_get());
    }

  void
    TypeChecker::operator()(ast::AssignExp& e)
    {
      // FIXED: Some code was deleted here.
      type(e.var_get());
      type(e.value_get());
      const auto def = dynamic_cast<const ast::SimpleVar*>(&e.var_get());
      if (def && var_read_only_.has(def->def_get()))
        error(e.var_get(), "variable is read only");
      else
      { 
        check_types(e, "var", e.var_get().type_get()->actual(), "value",
            e.value_get().type_get()->actual());
      }
      type_default(e, &(Void::instance()));
    }

  void
    TypeChecker::operator()(ast::StringExp& e)
    {
      // FIXED: Some code was deleted here.
      type_default(e, &(String::instance()));
    }

  void
    TypeChecker::operator()(ast::ForExp& e)
    {
      // FIXED: Some code was deleted here.
      type(e.vardec_get());
      var_read_only_.insert(&(e.vardec_get()));
      type(e.hi_get());
      type(e.body_get());
      check_type(e.vardec_get(), "for low", Int::instance());
      check_type(e.hi_get(), "for high", Int::instance());
      check_type(e.body_get(), "for body", Void::instance());
      type_default(e, &(Void::instance()));
    }

  void
    TypeChecker::operator()(ast::IfExp& e)
    {
      // FIXED: Some code was deleted here.
      type(e.cond_get());
      check_type(e.cond_get(), "int", Int::instance());
      type(e.then_exp_get());
      if (e.else_exp_get() != nullptr) {
        type(*(e.else_exp_get()));
        check_types(e, "then exp", e.then_exp_get(), "else exp", *(e.else_exp_get()));
      }
      else
        check_type(e.then_exp_get(), "then exp", Void::instance());
      type_default(e, e.then_exp_get().type_get());
    }

  void
    TypeChecker::operator()(ast::WhileExp& e)
    {
      // FIXED: Some code was deleted here.
      type(e.test_get());
      type(e.body_get());
      check_type(e.test_get(), "condition", Int::instance());
      check_type(e.body_get(), "body", Void::instance());
      type_default(e, &(Void::instance()));
    }

  void
    TypeChecker::operator()(ast::LetExp& e)
    {
      // FIXED: Some code was deleted here.
      super_type::operator()(e.let_get());
      type(e.in_get());
      type_default(e, e.in_get().type_get());
    }

  void
    TypeChecker::operator()(ast::SeqExp& e)
    {
      // FIXED: Some code was deleted here.
      if (e.exps_get().size() == 0)
      {
        type_default(e, &(Void::instance()));
      }
      else
      {
        for (int i = 0; i < e.exps_get().size(); ++i)
        {
          type(*(e.exps_get()[i]));
        }
        type_default(e, e.exps_get()[e.exps_get().size() - 1]->type_get());
      }
    }

  void
    TypeChecker::operator()(ast::CallExp& e)
    {
      const auto child = dynamic_cast<const Function*>(e.def_get()->type_get());
      if (!child)
      {
        error(e, "Not defined function");
        type_default(e, &Void::instance());
      }
      else
      {
        if (e.arg_list_get().size() !=
            child->formals_get().fields_get().size())
        {
          error(e, "Invalid number of argument");
          type_default(e, &Void::instance());
        }
        else
        {
          for (unsigned i = 0; i < e.arg_list_get().size(); i++)
          {
            auto arg = type(*e.arg_list_get()[i]);
            if (!(arg->compatible_with(
                    child->formals_get().fields_get()[i].type_get())))
            {
              error(e, "Incompatible argument type");
              type_default(e, &Void::instance());
            }
          }
        }
        type_set(e, &child->result_get());
      }
      //type_set(e, &child->result_get());
      //FIXME
    }

  void
    TypeChecker::operator()(ast::MethodCallExp& e)
    {
      type(e.object_get());
      const auto ty = dynamic_cast<const Class*>(e.object_get().type_get());
      if (ty)
      {
        Method *method;
        bool method_exist = false;
        int i = 0;
        while (!method_exist && i < ty->meths_get().size())
        {
          if (ty->meths_get()[i]->name_get() != e.name_get())
          {
            method = const_cast<Method*>(ty->meths_get()[i]);
            method_exist = true;
          }
          ++i;
        }
        if (!method_exist)
        {
          error(e, "The required method does not existin the object class.");
          type_default(e, &Void::instance());
        }
        else
        {
          if (e.arg_list_get().size() != method->formals_get().fields_get().size())
          {
            error(e, "The required method does not existin the object class.");
            type_default(e, &Void::instance());
          }
          else
          {
            bool correct_args = true;
            i = 0;
            while (correct_args && i < e.arg_list_get().size())
            {
              auto arg = type(*e.arg_list_get()[i]);
              if (*arg != method->formals_get().fields_get()[i].type_get())
              {
                correct_args = false;
                error(e, "Incompatible argument type");
                type_default(e, &Void::instance());
              }
              ++i;
            }
          }
        }
      }
      else
      {
        error(e, "Method object is not valid..");
        type_default(e, &Void::instance());
      }
    }


  void
    TypeChecker::operator()(ast::CastExp& e)
    {
      //FIXED
      type(e.ty_get());
      type_default(e, e.ty_get().type_get());
    }

  void
    TypeChecker::operator()(ast::BreakExp& e)
    {
      type_default(e, &(Void::instance()));
    }

  void
    TypeChecker::operator()(ast::ObjectExp& e)
    {
      //FIXME
      const auto type = dynamic_cast<const Class*>(&e.type_name_get());
      if (type)
        type_default(e, type);
      else
      {
        error(e, "Object Class does not exist.");
        type_default(e, &Void::instance());
      }

    }

  // Complex values.

  void TypeChecker::operator()(ast::RecordExp& e)
  {
    bool err = false;
    auto tn = type(e.type_name_get());
    const auto child = dynamic_cast<const Record*>(&tn->actual());
    if (!child)
    {
      err = true;
      error(e, "This node is not a record");
    }
    if (e.ft_get().size() != child->fields_get().size())
    {
      err = true;
      error(e, "Missing fields");
    }

    for (auto i = 0; !err && i < e.ft_get().size(); i++) // check each field
    {
      auto c = type(e.ft_get()[i]->init_get());
      if (child->fields_get()[i].name_get() == e.ft_get()[i]->name_get())
      { 
        if (child->fields_get()[i].type_get() != *c)
        {
          const auto child3 = dynamic_cast<const Nil*>(c);
          if (child3)
          {
            child3->set_record_type(child->fields_get()[i].type_get());
          }
          else
          {
            err = true;
            error(e, "Incompatible record type");
          }
        }
      }
      else
      {
        err = true;
        error(e, "Invalid record name");
      }
    }
    e.type_set(child);  

    // If no error occured, check for nil types. in the record initialization.
    // If any errors occured, there's no need to set any nil types.
    // If there are any record initializations, set the `record_type_`
    // of the `Nil` to the expected type.
    if (err) // error occurds
    {
      const auto child2 = dynamic_cast<const Nil*>(tn);
      if (child2)
        child2->set_record_type(Void::instance());
    }

    // FIXME: Some code was deleted here.
  }

  void
    TypeChecker::operator()(ast::OpExp& e)
    {
      // FIXMED: Some code was deleted here.

      // If any of the operands are of type Nil, set the `record_type_` to the
      // type of the opposite operand.
      type_set(e, &(Int::instance()));
      type(e.left_get());
      type(e.right_get());

      const auto nil_left = dynamic_cast
        <const Nil*>(&e.left_get().type_get()->actual());
      const auto nil_right = dynamic_cast
        <const Nil*>(&e.right_get().type_get()->actual());

      if (nil_left)
        e.left_get().type_set(e.right_get().type_get());
      if (nil_right)
        e.right_get().type_set(e.left_get().type_get());


      if (!error_)
      {
        const auto nil_left = dynamic_cast
          <const Nil*>(&e.left_get().type_get()->actual());
        const auto  array_left = dynamic_cast
          <const Array*>(&e.left_get().type_get()->actual());
        const auto record_left = dynamic_cast
          <const Record*>(&e.left_get().type_get()->actual());
        const auto int_left = dynamic_cast
          <const Int*>(&e.left_get().type_get()->actual());
        const auto string_left = dynamic_cast
          <const String*>(&e.left_get().type_get()->actual());

        const auto nil_right = dynamic_cast
          <const Nil*>(&e.right_get().type_get()->actual());
        const auto  array_right = dynamic_cast
          <const Array*>(&e.right_get().type_get()->actual());
        const auto record_right = dynamic_cast
          <const Record*>(&e.right_get().type_get()->actual());
        const auto int_right = dynamic_cast
          <const Int*>(&e.right_get().type_get()->actual());
        const auto string_right = dynamic_cast
          <const String*>(&e.right_get().type_get()->actual());

        if (e.oper_get() == ast::OpExp::Oper::add ||
            e.oper_get() == ast::OpExp::Oper::sub ||
            e.oper_get() == ast::OpExp::Oper::mul ||
            e.oper_get() == ast::OpExp::Oper::div)
        {
          if (!(int_left && int_right))
            //type_mismatch
            type_mismatch(e, "left", *e.left_get().type_get(), "right",
                *e.right_get().type_get());
        }

        if (e.oper_get() == ast::OpExp::Oper::lt ||
            e.oper_get() == ast::OpExp::Oper::le ||
            e.oper_get() == ast::OpExp::Oper::gt ||
            e.oper_get() == ast::OpExp::Oper::ge)
        {
          if (!((int_left && int_right) || (string_left && string_right)))
            //type_mismatch
            type_mismatch(e, "left", *e.left_get().type_get(), "right",
                *e.right_get().type_get());
        }

        if (e.oper_get() == ast::OpExp::Oper::eq ||
            e.oper_get() == ast::OpExp::Oper::ne)
        {
          if (!((int_left && int_right) || (string_left && string_right) ||
                (array_left && array_right) || (record_left && record_right) ||
                (record_left && nil_right) || (nil_left && record_right)))
          {
            //type_mismatch
            type_mismatch(e, "left", *e.left_get().type_get(), "right",
                *e.right_get().type_get());
          }
          if (array_left && array_right &&
              !array_left->compatible_with(*array_right))
          {
            //type_mismatch
            type_mismatch(e, "left", *e.left_get().type_get(), "right",
                *e.right_get().type_get());
          }
          if (record_left && record_right &&
              !record_left->compatible_with(*record_right))
          {
            //type_mismatch
            type_mismatch(e, "left", *e.left_get().type_get(), "right",
                *e.right_get().type_get());
          }


        }
        // FIXED: Some code was deleted here.
      }
    }

  // FIXME: Some code was deleted here.


  /*-----------------.
    | Visiting /Dec/.  |
    `-----------------*/

  /*------------------------.
    | Visiting FunctionDecs.  |
    `------------------------*/

  void
    TypeChecker::operator()(ast::FunctionDecs& e)
    {
      decs_visit<ast::FunctionDec>(e);
    }


  void
    TypeChecker::operator()(ast::FunctionDec&)
    {
      // We must not be here.
      unreachable();
    }


  // Store the type of this function.
  template <>
    void
    TypeChecker::visit_dec_header<ast::FunctionDec>(ast::FunctionDec& e)
    {
      const Record* formals = type(e.formals_get());
      if (e.result_get() == nullptr)
      {
        auto tmp = new Function(formals, Void::instance());
        e.created_type_set(tmp);
        type_set(e, tmp);
      }
      else
      {
        auto tmp = new Function(formals, *type(*e.result_get()));
        e.created_type_set(tmp);
        type_set(e, tmp);
      }
      // FIXED: Some code was deleted here.
    }


  // Type check this function's body.
  template <>
    void
    TypeChecker::visit_dec_body<ast::FunctionDec>(ast::FunctionDec& e)
    {
      visit_routine_body<Function>(e);

      // Check for Nil types in the function body.
      if (!error_ && e.body_get())
      {
        type(*e.body_get());
        const auto child = dynamic_cast<const Nil*>(e.body_get()->type_get());
        if (child) // correct ?
        {
          const auto child2 = dynamic_cast<const
            Function*>(e.created_type_get());
          if (child2)
            child->set_record_type(child2->result_get());
        }
        // FIXED: Some code was deleted here.
      }
    }


  /*---------------.
    | Visit VarDec.  |
    `---------------*/ 
  void
    TypeChecker::operator()(ast::VarDec& e)
    {
      // FIXED: Some code was deleted here.
      if (e.type_name_get())
      { // var a : int = 3
        type(*(e.type_name_get()));
        type_default(e, e.type_name_get()->type_get());
        if (e.init_get() != nullptr)
        {
          type(*(e.init_get()));
          check_types(e, "node", (e.type_name_get()->type_get()->actual()), "init",
              (e.init_get()->type_get()->actual()));

         /* const auto child = dynamic_cast<const Nil*>(e.init_get()->type_get());
          if (child)
          {
            error(e, "assign nil to default var");
          }*/

        }
      }
      else if (e.init_get() != nullptr)
      { // var a = 3
        type(*(e.init_get()));
        type_default(e, e.init_get()->type_get());

        const auto child = dynamic_cast<const Nil*>(e.init_get()->type_get());
        if (child)
        {
          error(e, "assign nil to default var");
        }
      }

    }

  /*--------------------.
    | Visiting TypeDecs.  |
    `--------------------*/

  void
    TypeChecker::operator()(ast::TypeDecs& e)
    {
      decs_visit<ast::TypeDec>(e);
    }


  void
    TypeChecker::operator()(ast::TypeDec&)
    {
      // We must not be here.
      unreachable();
    }


  // Store this type.
  template <>
    void
    TypeChecker::visit_dec_header<ast::TypeDec>(ast::TypeDec& e)
    {
      // We only process the head of the type declaration, to set its
      // name in E.  A declaration has no type in itself; here we store
      // the type declared by E.
      auto type = new Named(e.name_get());
      e.created_type_set(type);
      type_set(e, type);
      // FIXED: Some code was deleted here.
    }

  // Bind the type body to its name.
  template <>
    void
    TypeChecker::visit_dec_body<ast::TypeDec>(ast::TypeDec& e)
    {
      const auto child = dynamic_cast<const Named*>(e.type_get());
      if (child)
      {
        //  e.ty_get().type_set(type);
        auto t = type(e.ty_get());
        child->type_set(t);
        if (!child->sound())
          error(e, "Infiny record type recursion");
      }
      else
      {
        error(e, "This node is a correct type dec");
      }
      // FIXED: Some code was deleted here.
    }

  /*------------------.
    | Visiting /Decs/.  |
    `------------------*/

  template <class D>
    void
    TypeChecker::decs_visit(ast::AnyDecs<D>& e)
    {
      for (auto it = e.decs_get().begin(); it != e.decs_get().end(); it++)
      {
        visit_dec_header(**it);
      }
      for (auto it = e.decs_get().begin(); it != e.decs_get().end(); it++)
        visit_dec_body(**it);
      // FIXED: Some code was deleted here.
    }


  /*-------------.
    | Visit /Ty/.  |
    `-------------*/

  void
    TypeChecker::operator()(ast::NameTy& e)
    {
      // FIXED: Some code was deleted here (Recognize user defined types, and built-in types).
      if (e.def_get() != nullptr)
        type_default(e, e.def_get()->type_get());
      else if (e.name_get() == "int")
        type_default(e, &(Int::instance()));
      else if (e.name_get() == "string")
        type_default(e, &(String::instance()));
    }

  void
    TypeChecker::operator()(ast::RecordTy& e)
    {
      // FIXME: Some code was deleted here.
      auto t = type(e.ft_get());
      e.created_type_set(t);
      type_set(e, t);

    }

  void
    TypeChecker::operator()(ast::ArrayTy& e)
    {
      // FIXED: Some code was deleted here.
      type(e.base_type_get());
      created_type_default(e, new Array(e.base_type_get().type_get()));
      type_default(e, e.created_type_get());
    }

}
