/**
 ** \file type/function.cc
 ** \brief Implementation for type/function.hh.
 */

#include <ostream>

#include <type/function.hh>
#include <type/visitor.hh>

namespace type
{

  void
  Function::accept(ConstVisitor& v) const
  {
    v(*this);
  }

  void
  Function::accept(Visitor& v)
  {
    v(*this);
  }
  
  bool Function::compatible_with(const Type& other) const
  {
    const auto child = dynamic_cast<const Function*>(&other);
    if (child && result_ == child->result_get()
      && *formals_ == child->formals_get())
      return true;
    return false;
  }
  // FIXME: Some code was deleted here.

} // namespace type
