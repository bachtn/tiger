/**
 ** \file type/record.cc
 ** \brief Implementation for type/record.hh.
 */

#include <ostream>

#include <type/builtin-types.hh>
#include <type/visitor.hh>
#include <type/record.hh>
#include <type/nil.hh>

namespace type
{

  void
    Record::accept(ConstVisitor& v) const
    {
      v(*this);
    }

  void
    Record::accept(Visitor& v)
    {
      v(*this);
    }

  const Type* Record::field_type(const misc::symbol key) const
  {
    int i = field_index(key);
    if (i != -1)
      return &(fields_[i].type_get());
    return nullptr;
  }

  /* return -1 if the key has not been found */
  int Record::field_index(const misc::symbol key) const
  {
    for (unsigned i = 0; i < fields_.size(); i++)
    {
      if (fields_[i].name_get() == key)
        return i;
    }
    return -1;
  }
  // FIXME: Some code was deleted here (Field manipulators).


  bool Record::compatible_with(const Type& other) const
  {
    return this->actual() == other.actual() || dynamic_cast<const Nil*>(&other);
    /*else
    {
      const auto child2 = dynamic_cast<const Record*>(&other);
      if (child2)
      {
        const fields_type f = child2->fields_get();
        if (fields_.size() != f.size())
          return false;
        for (unsigned i = 0; i < fields_.size(); i++)
        {
          if (fields_[i].type_get() != f[i].type_get())
              return false;
        }
        return true;
      }
      return false;
      // FIXME: Some code was deleted here (Special implementation of "compatible_with" for Record).
    }*/
  }

  } // namespace type
