/**
 ** \file type/named.cc
 ** \brief Implementation for type/named.hh.
 */

#include <ostream>

#include <type/named.hh>
#include <type/visitor.hh>

namespace type
{

  void Named::accept(ConstVisitor& v) const
  {
    v(*this);
  }
  void Named::accept(Visitor& v)
  {
    v(*this);
  }


  /** \brief Whether the definition of this named type is sound,
   ** i.e. that there is no recursive dependency.  */
  bool Named::sound() const
  {
    std::set<const Named*> set = std::set<const Named*>();
    for (const Named* cur = this; cur; cur = dynamic_cast<const
      Named*>(cur->type_get()))
    {
      if (set.find(cur) != set.end())
        return false;
      set.insert(cur);
    }
    return true;
  }
  /** \} */

  bool Named::compatible_with(const Type& other) const
  {
    return this->actual().compatible_with(other);
  }


} // namespace type
