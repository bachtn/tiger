/**
 ** \file type/type.hxx
 ** \brief Inline methods for type::Type.
 */
#pragma once

#include <type/type.hh>
#include <misc/contract.hh>

namespace type
{
  inline bool
  operator==(const Type& lhs, const Type& rhs)
  {
    return &(lhs.actual()) == &(rhs.actual());
  // FIXME: Some code was deleted here.
  }

  inline bool
  operator!=(const Type& lhs, const Type& rhs)
  {
    return !(lhs == rhs);
  // FIXME: Some code was deleted here.
  }

} // namespace type
