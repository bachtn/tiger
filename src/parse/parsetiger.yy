                                                                // -*- C++ -*-
%require "3.0"
%language "C++"
// Set the namespace name to `parse', instead of `yy'.
%name-prefix "parse"
// The type of semantic values set to variant
%define api.value.type variant
// Request that symbols be handled as a whole in the scanner.
%define api.token.constructor

%skeleton "glr.cc"  // The grammar is GLR
%glr-parser
%expect 1             // 1 shift/reduce
%expect-rr 0
%error-verbose        // Put a specific message in case of parse error
%defines              // Generate header files which can be used by the scanner
%debug                // Debug Mode

// Prefix all the tokens with TOK_ to avoid colisions.
%define api.token.prefix {TOK_}

/* We use pointers to store the filename in the locations.  This saves
   space (pointers), time (no deep copy), but leaves the problem of
   deallocation.  This would be a perfect job for a misc::symbol
   object (passed by reference), however Bison locations requires the
   filename to be passed as a pointer, thus forcing us to handle the
   allocation and deallocation of this object.

   Nevertheless, all is not lost: we can still use a misc::symbol
   object to allocate a flyweight (constant) string in the pool of
   symbols, extract it from the misc::symbol object, and use it to
   initialize the location.  The allocated data will be freed at the
   end of the program (see the documentation of misc::symbol and
   misc::unique).  */
%define filename_type {const std::string}
%locations

// The parsing context.
%param { ::parse::TigerParser& tp }

/*---------------------.
| Support for tokens.  |
`---------------------*/
%code requires
{
#include <string>
#include <misc/algorithm.hh>
#include <misc/separator.hh>
#include <misc/symbol.hh>
#include <parse/fwd.hh>

  // Pre-declare parse::parse to allow a ``reentrant'' parsing within
  // the parser.
  namespace parse
  {
    ast_type parse(Tweast& input);
  }
}

%code provides
{
  // Announce to Flex the prototype we want for lexing (member) function.
  # define YY_DECL_(Prefix)                               \
    ::parse::parser::symbol_type                          \
    (Prefix parselex)(::parse::TigerParser& tp)
  # define YY_DECL YY_DECL_(yyFlexLexer::)
}

%token <std::string>    STR "string"
%token <misc::symbol>   ID     "id"
%token <int>            INT    "integer"


/*--------------------------------.
| Support for the non-terminals.  |
`--------------------------------*/

%code requires
{
# include <ast/fwd.hh>
// Provide the declarations of the following classes for the
// %destructor clauses below to work properly.
# include <ast/exp.hh>
# include <ast/var.hh>
# include <ast/ty.hh>
# include <ast/name-ty.hh>
# include <ast/field.hh>
# include <ast/field-init.hh>
# include <ast/function-dec.hh>
# include <ast/method-dec.hh>
# include <ast/type-dec.hh>
# include <ast/var-dec.hh>
# include <ast/any-decs.hh>
# include <ast/decs-list.hh>
}

%printer { yyo << $$; } <int> <std::string> <misc::symbol>

%destructor { delete $$; } <ast::Exp*> <ast::DecsList*> <ast::Decs*>
                           <ast::Var*>
                           <ast::Ty*> <ast::NameTy*>
                           <ast::FunctionDec*> <ast::MethodDec*> <ast::TypeDec*>

%destructor { misc::deep_clear(*$$); delete $$; } <ast::fields_type*> <ast::exps_type*>
                          <ast::fieldinits_type*>

%printer { yyo << $$; } <ast::Exp*> <ast::DecsList*> <ast::Decs*>
                        <ast::exps_type*> <ast::Var*> <ast::fieldinits_type*>
                        <ast::Ty*> <ast::fields_type*> <ast::NameTy*>
                        <ast::FunctionDec*> <ast::MethodDec*> <ast::TypeDec*>
                        <ast::FunctionDecs*> <ast::MethodDecs*> <ast::TypeDecs*>
                        <ast::VarDecs*>;

/*-----------------------------------------.
| Code output in the implementation file.  |
`-----------------------------------------*/

%code
{
# include <parse/tiger-parser.hh>
# include <parse/scantiger.hh>
# include <parse/tweast.hh>
# include <misc/separator.hh>
# include <misc/symbol.hh>
# include <ast/all.hh>
# include <ast/libast.hh>

  namespace
  {
    /// Get the metavar from the specified map.
    template <typename T>
    T*
    metavar(parse::TigerParser& tp, unsigned key)
    {
      parse::Tweast* input = tp.input_;
      return input->template take<T>(key);
    }

  }

  /// Use our local scanner object.
  inline
  ::parse::parser::symbol_type
  parselex(parse::TigerParser& tp)
  {
    return tp.scanner_->parselex(tp);
  }
}

// Definition of the tokens, and their pretty-printing.
/* Keywords */
%token ARRAY        "array"
      IF           "if"
      THEN         "then"
      ELSE         "else"
      WHILE        "while"
      FOR          "for"
      TO           "to"
      DO           "do"
      LET          "let"
      IN           "in"
      END          "end"
      OF           "of"   /* Changed it "OF" before change */
      BREAK        "break"
      NIL          "nil"
      FUNCTION     "function"
      VAR          "var"
      TYPE         "type"
      IMPORT       "import"
      PRIMITIVE    "primitive"

/* keywords for object-oriented constructions */
      CLASS        "class"
      EXTENDS      "extends"
      METHOD       "method"
      NEW          "new"

/* Additional keywords and identifiers */
      DECS         "_decs"
      CAST         "_cast"
      EXP          "_exp"
      LVALUE       "_lvalue"
      NAMETY       "_namety"

/* Symbols */
      COMMA        ","
      COLON        ":"
      SEMICOLON    ";"
      LPAREN       "("
      RPAREN       ")"
      LSQBRAC      "["
      RSQBRAC      "]"
      LBRAC        "{"
      RBRAC        "}"
      FULLSTOP     "."
      PLUS         "+"
      MINUS        "-"
      STAR         "*"
      SLASH        "/"
      EQUAL        "="
      DIFFERENT    "<>"
      LOW          "<"
      LOWEQUAL     "<="
      UPPER        ">"
      UPEQUAL      ">="
      AND          "&"
      OR           "|"
      RECEIVE      ":="
      QUEST        "?"

/* White caracters */
      SPACE        " "
      TAB          "\t"

/* End of line */
      EOL          "\n"
/* End of file */
      EOF 0 "<EOF>" // Token EOF, automaticaly managed by the parser

%type <ast::Exp*>             exp
%type <ast::DecsList*>        decs
%type <ast::Decs*>            dec
%type <ast::exps_type*>       exp1
%type <ast::exps_type*>       exp2
%type <ast::exps_type*>       exps
%type <ast::exps_type*>       exp3
%type <ast::Var*>             lvalue
%type <ast::Var*>             lvalue1
%type <ast::fieldinits_type*> id1
%type <ast::fieldinits_type*> id2
%type <ast::Ty*>              ty
%type <ast::fields_type*>     tyfields
%type <ast::fields_type*>     id3
%type <ast::VarDecs*>         vardecs
%type <ast::VarDecs*>         vardecs1
%type <ast::VarDecs*>         vardec
%type <ast::DecsList*>        classfields
%type <ast::Decs*>            classfield
%type <ast::NameTy*>          typeid
%type <ast::NameTy*>          typeid1
%type <ast::NameTy*>          extends1
%type <ast::TypeDecs*>        type_decs
%type <ast::TypeDec*>         type_dec
%type <ast::FunctionDecs*>    fct_decs
%type <ast::FunctionDec*>     fct_dec
%type <ast::MethodDecs*>      meth_decs
%type <ast::MethodDec*>       meth_dec





/* Priorities */
%nonassoc DO OF THEN QUEST COLON
%precedence ELSE
%precedence RECEIVE
%left OR
%left AND
%nonassoc EQUAL DIFFERENT LOWEQUAL UPEQUAL UPPER LOW
%left PLUS MINUS
%left STAR SLASH
%nonassoc DECS
%nonassoc VAR CLASS PRIMITIVE FUNCTION METHOD TYPE

/* Start Program */
%start program

%%
program:
  /* Parsing a source program.  */
  exp                                           { tp.ast_ = $1;                                       }
| /* Parsing an imported file.  */
  decs                                          { tp.ast_ = $1;                                       }
;

exp:
  /* Literals */
  INT                                           { $$ = new ast::IntExp(@$, $1);                       }
  | NIL                                         { $$ = new ast::NilExp(@$); }
  | STR                                         { $$ = new ast::StringExp(@$, $1);                    }

  /* Cast of an expression to a given type */
  | CAST LPAREN exp COMMA ty RPAREN             { $$ = new ast::CastExp(@$, $3, $5);                  }

  /* An expression metavariable */
  | EXP LPAREN INT RPAREN                       { $$ = metavar<ast::Exp>(tp, $3);                     }

  /* Array and record creations */
  | ID LSQBRAC exp RSQBRAC OF exp               {
                                                  $$ = new ast::ArrayExp
                                                  (@$, new ast::NameTy(@$, $1), $3, $6);
                                                }
  | ID LBRAC id1 RBRAC                          {
                                                  $$ = new ast::RecordExp(@$,
                                                  new ast::NameTy(@$, $1), $3);
                                                }

  /* Object creation */
  | NEW typeid                                  { $$ = new ast::ObjectExp(@$, $2);                   }

  /* Variables, field, elements of an array */
  | lvalue                                      { $$ = $1;                                           }

  /* Function call */
  | ID LPAREN exp1 RPAREN                       { $$ = new ast::CallExp(@$, $1, $3);                 }

  /* Method call */
  | lvalue FULLSTOP ID LPAREN exp1 RPAREN       { $$ = new ast::MethodCallExp(@$, $1, $3, $5);       }

  /* Operations */
  | MINUS exp                                   {
                                                  $$ = new ast::OpExp(@$, new ast::IntExp(@$, 0),
                                                               ast::OpExp::sub, $2);
                                                }
  | exp STAR exp                                { $$ = new ast::OpExp(@$, $1, ast::OpExp::mul, $3);  }
  | exp SLASH exp                               { $$ = new ast::OpExp(@$, $1, ast::OpExp::div, $3);  }
  | exp PLUS exp                                { $$ = new ast::OpExp(@$, $1, ast::OpExp::add, $3);  }
  | PLUS PLUS lvalue                            {
                                                  $$ =
                                                  parse::parse(parse::Tweast() << $3 << " = " << $3 << "+ 1"); 
                                                }
  | exp MINUS exp                               { $$ = new ast::OpExp(@$, $1, ast::OpExp::sub, $3);  }
  | exp EQUAL exp                               { $$ = new ast::OpExp(@$, $1, ast::OpExp::eq, $3);   }
  | exp UPPER exp                               { $$ = new ast::OpExp(@$, $1, ast::OpExp::gt, $3);   }
  | exp UPEQUAL exp                             { $$ = new ast::OpExp(@$, $1, ast::OpExp::ge, $3);   }
  | exp DIFFERENT exp                           { $$ = new ast::OpExp(@$, $1, ast::OpExp::ne, $3);   }
  | exp LOW exp                                 { $$ = new ast::OpExp(@$, $1, ast::OpExp::lt, $3);   }
  | exp LOWEQUAL exp                            { $$ = new ast::OpExp(@$, $1, ast::OpExp::le, $3);   }
  | exp AND exp                                 {
                                                  $$ = parse::parse(parse::Tweast() << "if " << $1 <<
                                                  " then " << $3 << "<> 0 else 0");
                                                }
  | exp OR exp                                  {
                                                  $$ = parse::parse(parse::Tweast() << "if " << $1 <<
                                                  " then 1 else " << $3 << "<> 0");
                                                }
  | LPAREN exps RPAREN                          { $$ = new ast::SeqExp(@$, $2);                      }
  | LPAREN error RPAREN                         { $$ = new ast::SeqExp(@$, new ast::exps_type());    }

  /* Assignment */
  | lvalue RECEIVE exp                          { $$ = new ast::AssignExp(@$, $1, $3);               }

  /* Control structures */
  | IF exp THEN exp                             { $$ = new ast::IfExp(@$, $2, $4);                   }
  | IF error THEN exp                           {
                                                  $$ = new ast::IfExp(@$, new
                                                  ast::NilExp(@$), $4);
                                                }
  | exp QUEST exp COLON exp                     { $$ = new ast::IfExp(@$, $1, $3, $5);               } 
  | IF exp THEN exp ELSE exp                    { $$ = new ast::IfExp(@$, $2, $4, $6);               }
  | WHILE exp DO exp                            { $$ = new ast::WhileExp(@$, $2, $4);                }




  | FOR ID RECEIVE exp TO exp DO exp            {
                                                  $$ = new ast::ForExp(@$, new
                                                  ast::VarDec(@$, $2, nullptr, $4) , $6, $8);
                                                }
  | BREAK                                       { $$ = new ast::BreakExp(@$);                        }
  | LET decs IN exps END                        {
                                                  $$ = new ast::LetExp(@$, $2, new
                                                  ast::SeqExp(@$, $4));
                                                }
  | LET decs IN error END                       {
                                                  $$ = new ast::LetExp(@$, $2, new
                                                  ast::SeqExp(@$, new ast::exps_type()));
                                                }
;

/* lvalue */
lvalue : ID                                     { $$ = new ast::SimpleVar(@$, $1);                   }
       /* Cast of a lvalue to a given type */
       | CAST LPAREN lvalue COMMA ty RPAREN     { $$ = new ast::CastVar(@$, $3, $5);                 }
       /* A lvalue metavariable */
       | LVALUE LPAREN INT RPAREN               { $$ = metavar<ast::Var>(tp, $3);                    }
       | lvalue1                                { $$ = $1;                                           }
       ;

lvalue1 : lvalue FULLSTOP ID                    { $$ = new ast::FieldVar(@$, $1, $3);                }
        | ID LSQBRAC exp RSQBRAC                {
                                                  $$ = new ast::SubscriptVar(@$,
                                                       new ast::SimpleVar(@$, $1) ,
                                                       $3);
                                                }
        | lvalue1 LSQBRAC exp RSQBRAC           { $$ = new ast::SubscriptVar(@$, $1, $3);            }
        ;



id1 : %empty                                    { $$ = new ast::fieldinits_type();                   }
    | ID EQUAL exp id2                          {
                                                  $4->insert($4->begin(),
                                                  new ast::FieldInit(@$, $1, $3));
                                                  $$ = $4;
                                                }

    ;

id2 : %empty                                    { $$ = new ast::fieldinits_type();                    }
      | COMMA ID EQUAL exp id2                  {
                                                  $5->insert($5->begin(),
                                                  new ast::FieldInit(@$, $2, $4));
                                                  $$ = $5;
                                                }
      ;

exp1 : %empty                                   { $$ = new ast::exps_type();                          }
     | exp exp2                                 {
                                                  $2->insert($2->begin(), $1);
                                                  $$ = $2;
                                                }
     ;

exp2 : %empty                                   { $$ = new ast::exps_type();                          }
       | COMMA exp exp2                         {
                                                  $3->insert($3->begin(), $2);
                                                  $$ = $3;
                                                }
       ;

exps : %empty                                   { $$ = new ast::exps_type();                          }
      | exp exp3                                { $2->insert($2->begin(), $1); $$ = $2;               }
      ;

exp3 : %empty                                   { $$ = new ast::exps_type();                          }
       | SEMICOLON exp exp3                     { $3->insert($3->begin(), $2); $$ = $3;               }
       ;





id3 : %empty                                    { $$ = new ast::fields_type();                        }
      | COMMA ID COLON ID id3                   {
                                                  $5->insert($5->begin(), new ast::Field(@$, $2,
                                                  new ast::NameTy(@$, $4)));
                                                  $$ = $5;
                                                }
      ;

/*---------------.
|     Types.     |
`---------------*/

ty : typeid                                     { $$ = $1;                                            }
   | LBRAC tyfields RBRAC                       { $$ = new ast::RecordTy(@$, $2);                     }
   | ARRAY OF typeid                            { $$ = new ast::ArrayTy(@$, $3);                      }
   | CLASS extends1 LBRAC classfields RBRAC     { $$ = new ast::ClassTy(@$, $2, $4);                  }
   ;

typeid : ID                                     { $$ = new ast::NameTy(@$, $1);                       }
      | NAMETY LPAREN INT RPAREN                { $$ = metavar<ast::NameTy>(tp, $3);                  }
      ;

tyfields : %empty                               { $$ = new ast::fields_type();                        }
     | ID COLON typeid id3                      {
                                                  $4->insert($4->begin(), new ast::Field(@$, $1, $3));
                                                  $$ = $4;
                                                }
     ;

/*---------------.
| Declarations.  |
`---------------*/

decs:
  %empty                                        { $$ = new ast::DecsList(@$);                         }
  /* A list of decs metavariables */
  | DECS LPAREN INT RPAREN decs                 {
                                                  auto* t = metavar<ast::DecsList>(tp, $3);
                                                  $5->splice_front(*t);
                                                  delete t;
                                                  $$ = $5; }
  | dec decs                                    {
                                                  $2->push_front($1);
                                                  $$ = $2;
                                                }
  | IMPORT STR decs                             {
                                                  auto* t = tp.parse_import($2, @$);
                                                  if (t)
                                                  {
                                                    $3->splice_front(*t);
                                                    delete t;
                                                  }
                                                  $$ = $3;
                                                }
  ;

dec :
  vardec                                        { $$ = $1;                                            }
  | type_decs                                   { $$ = $1;                                            }
  | fct_decs                                    { $$ = $1;                                            }
  ;

vardec : VAR ID typeid1 RECEIVE exp             {
                                                  $$ = new ast::VarDecs(@$,
                                                       new std::vector<ast::VarDec*>({new ast::VarDec(@$,
                                                       $2, $3, $5)}));
                                                }
       ;

            ;

classfield : vardec                             { $$ = $1;                                            }
           | meth_decs                          { $$ = $1;                                            }
          ;

classfields : %empty                            { $$ = new ast::DecsList(@$);                         }
             | classfield classfields           {
                                                  $2->push_front($1);
                                                  $$ = $2;
                                                }
             ;

vardecs : %empty                                { $$ = new ast::VarDecs(@$);                          }
     | ID COLON ID vardecs1                     { auto* v = new ast::VarDec(@$, $1,
                                                  new ast::NameTy(@$, $3), nullptr);
                                                  $4->push_front(*v);
                                                  $$ = $4;
                                                }
     ;


vardecs1 : %empty                               { $$ = new ast::VarDecs(@$);                          }
      | COMMA ID COLON ID vardecs1              {
                                                  auto* v = new ast::VarDec(@$, $2,
                                                  new ast::NameTy(@$, $4), nullptr);
                                                  $5->push_front(*v);
                                                  $$ = $5;
                                                }
      ;

type_decs: type_dec %prec DECS                  { $$ = new ast::TypeDecs(@$,
                                                       new std::vector<ast::TypeDec*>({$1}));
                                                }

          | type_dec type_decs                  {
                                                  $2->push_front(*$1);
                                                  $$ = $2;
                                                }
          ;

type_dec:
  TYPE ID EQUAL ty                              { $$ = new ast::TypeDec(@$, $2, $4); }
  | CLASS ID extends1 LBRAC classfields RBRAC   { $$ = new ast::TypeDec(@$, $2,
                                                  new ast::ClassTy(@$, $3, $5)); }
  ;

extends1 : %empty                               { $$ = new ast::NameTy(@$, "Object"); }
         | EXTENDS typeid                       { $$ = $2; }
         ;

fct_decs: fct_dec %prec DECS                    {
                                                  $$ = new ast::FunctionDecs(@$, new
                                                    std::vector<ast::FunctionDec*>({$1}));
                                                }
          | fct_dec fct_decs                    {
                                                  $2->push_front(*$1);
                                                  $$ = $2;
                                                }
          ;

fct_dec: FUNCTION ID LPAREN vardecs RPAREN typeid1 EQUAL exp
                                                {
                                                  $$ = new ast::FunctionDec(@$, $2, $4,
                                                  $6, $8);
                                                }
       | PRIMITIVE ID LPAREN vardecs RPAREN typeid1
                                                {
                                                  $$ = new ast::FunctionDec(@$, $2, $4,
                                                  $6, nullptr);
                                                }
          ;

typeid1 : %empty                                { $$ = nullptr;                                       }
        | COLON typeid                          { $$ = $2;                                            }
        ;

meth_decs: meth_dec %prec DECS                  {
                                                  $$ = new ast::MethodDecs(@$, new
                                                  std::vector<ast::MethodDec*>({$1}));
                                                }
          | meth_dec meth_decs                  {
                                                  $2->push_front(*$1);
                                                  $$ = $2;
                                                }
          ;

meth_dec: METHOD ID LPAREN vardecs RPAREN typeid1 EQUAL exp
                                                {
                                                  $$ = new ast::MethodDec(@$, $2, $4,
                                                  $6, $8);
                                                }
          ;
%%


// Parse error function, automaticaly call by the parser in case of parse error
void
parse::parser::error(const location_type&, const std::string& m)
{
  tp.error_ << misc::error::parse
            << tp.location_
            << ": "
            << m
            << std::endl;
}
