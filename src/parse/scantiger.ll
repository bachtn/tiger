                                                            /* -*- C++ -*- */
%option c++
%option nounput
%option debug
%option batch

%{
#define ERROR " : lexical error, "

# include <climits>

# include <regex>
# include <string>
# include <cerrno>

# include <boost/lexical_cast.hpp>

# include <misc/contract.hh>
# include <misc/symbol.hh>
  // Using misc::escape is very useful to quote non printable characters.
  // For instance
  //
  //    std::cerr << misc::escape('\n') << std::endl;
  //
  // reports about `\n' instead of an actual new-line character.
# include <misc/escape.hh>
# include <parse/tiger-parser.hh>
# include <parse/parsetiger.hh>

# define YY_USER_ACTION                   \
  {                                       \
    tp.location_.columns(strlen(yytext)); \
  }


// Useful typedef
/* using token = yy::parser::token;*/

// Convenient shortcuts.
#define TOKEN_VAL(Type, Value)                  \
  parser::make_ ## Type(Value, tp.location_)

#define TOKEN(Type)                             \
  parser::make_ ## Type(tp.location_)

// Flex uses `0' for end of file.  0 is not a token_type.
#define yyterminate() return TOKEN(EOF)

# define CHECK_EXTENSION()                              \
  do {                                                  \
    if (!tp.enable_extensions_p_)                       \
      tp.error_ << misc::error::scan                    \
                << tp.location_                         \
                << ERROR                                \
                << "invalid identifier: `"              \
                << misc::escape(yytext) << "'\n";       \
  } while (false)

# define CHECK_OBJ_EXTENSION()                              \
  do {                                                  \
    if (!tp.enable_object_extensions_p_)                       \
      tp.error_ << misc::error::scan                    \
                << tp.location_                         \
                << ERROR                                \
                << "invalid identifier: `"              \
                << misc::escape(yytext) << "'\n";       \
  } while (false)

YY_FLEX_NAMESPACE_BEGIN
%}
%x SC_COMMENT SC_STRING

/* Abbreviations.  */
int             [0-9]+
SPACE           [ \t]+
ID              ([a-zA-Z]([_a-zA-Z0-9])*|_main)
EOL             \n\r|\r\n|\n|\r
NUM             [0-3][0-7][0-7]
XNUM            [a-fA-F0-9][a-fA-F0-9]

%%
%{
  int comments = 0;
  std::string str = "";

  // Each time yylex is called.
  tp.location_.step();
%}

 /* The rules.  */

{int}         {
                int val = 0; /* Must check if the value can be stored in 32bit
                register */
                try
                {
                    val = boost::lexical_cast<int>(yytext);
                }
                catch(const boost::bad_lexical_cast &)
                {
                    tp.error_ << misc::error::scan
                    << tp.location_         
                    << ERROR
                    << "invalid number : "
                    << yytext << std::endl;
                }
                return TOKEN_VAL(INT, val);
              }

"\""            {
                  str.clear();
                  BEGIN(SC_STRING);
                }
<SC_STRING>
{
  "\""          {
                  BEGIN(INITIAL);
                  return TOKEN_VAL(STR, str);
                }
  "\\\""        { str.append("\""); }
  "\\\\"        { str.append("\\"); }
  "\\a"         { str.append(1, '\a'); }
  "\\b"         { str.append(1, '\b'); }
  "\\f"         { str.append(1, '\f'); }
  "\\n"         { str.append(1, '\n'); }
  "\\r"         { str.append(1, '\r'); }
  "\\t"         { str.append(1, '\t'); }
  "\\v"         { str.append(1, '\v'); }
  "\\"{NUM}     { str.append(1, strtol(yytext + 1, 0, 8)); }
  "\\x"{XNUM}   { str.append(1, strtol(yytext + 2, 0, 16)); }
  "\\".         {
                    tp.error_ << misc::error::scan
                    << tp.location_ 
                    << ERROR
                    << "invalid escape character"
                    << std::endl;
                    BEGIN(INITIAL);
                }
  <<EOF>>       {
                    tp.error_ << misc::error::scan
                    << tp.location_
                    << ERROR
                    << "invalid end of file in a string"
                    << std::endl;
                    BEGIN(INITIAL);
                }
  {EOL}         {
                }
  .             { str.append(yytext); }
}

"/*"            { BEGIN(SC_COMMENT); comments++; }
<SC_COMMENT>
{
  "/*"          { comments++; }
  "*/"          {
                  comments--;
                  if (comments == 0)
                    BEGIN(INITIAL);
                }
  <<EOF>>       {
                    tp.error_ << misc::error::scan
                    << tp.location_
                    << ERROR
                    << "unexpected end of file in a comment"
                    << std::endl;
                    BEGIN(INITIAL);
                }
  {EOL}          { }
  .             { }
}
"_cast"       {  return TOKEN (CAST);     }
"_decs"       {  return TOKEN (DECS);     }
"_exp"        {  return TOKEN (EXP);      }
"_lvalue"     {  return TOKEN (LVALUE);   }
"_namety"     {  return TOKEN (NAMETY);   }
"array"       {  return TOKEN(ARRAY);    }
"if"          {  return TOKEN (IF);       }
"then"        {  return TOKEN (THEN);     }
"else"        {  return TOKEN (ELSE);     }
"while"       {  return TOKEN (WHILE);    }
"for"         {  return TOKEN (FOR);      }
"to"          {  return TOKEN (TO);       }
"do"          {  return TOKEN (DO);       }
"let"         {  return TOKEN (LET);      }
"in"          {  return TOKEN (IN);       }
"end"         {  return TOKEN (END);      }
"of"          {  return TOKEN (OF);       }
"break"       {  return TOKEN (BREAK);    }
"nil"         {  return TOKEN (NIL);      }
"function"    {  return TOKEN (FUNCTION); }
"var"         {  return TOKEN (VAR);      }
"type"        {  return TOKEN (TYPE);     }
"import"      {  return TOKEN (IMPORT);   }
"primitive"   {  return TOKEN (PRIMITIVE);}
"class"       {  CHECK_OBJ_EXTENSION(); return TOKEN (CLASS);    }
"extends"     {  CHECK_OBJ_EXTENSION(); return TOKEN (EXTENDS);  }
"method"      {  CHECK_OBJ_EXTENSION(); return TOKEN (METHOD);   }
"new"         {  CHECK_OBJ_EXTENSION(); return TOKEN (NEW);      }
","           {  return TOKEN (COMMA);    }
":"           {  return TOKEN (COLON);    }
";"           {  return TOKEN (SEMICOLON);}
"("           {  return TOKEN (LPAREN);   }
")"           {  return TOKEN (RPAREN);   }
"["           {  return TOKEN (LSQBRAC);  }
"]"           {  return TOKEN (RSQBRAC);  }
"{"           {  return TOKEN (LBRAC);    }
"}"           {  return TOKEN (RBRAC);    }
"."           {  return TOKEN (FULLSTOP); }
"+"           {  return TOKEN (PLUS);     }
"-"           {  return TOKEN (MINUS);    }
"*"           {  return TOKEN (STAR);     }
"/"           {  return TOKEN (SLASH);    }
"="           {  return TOKEN (EQUAL);    }
"<>"          {  return TOKEN (DIFFERENT);}
">"           {  return TOKEN (UPPER);  }
"<"           {  return TOKEN (LOW);      }
">="          {  return TOKEN (UPEQUAL);  }
"<="          {  return TOKEN (LOWEQUAL); }
"&"           {  return TOKEN (AND);      }
"|"           {  return TOKEN (OR);       }
":="          {  return TOKEN (RECEIVE);  }
"?"           {  return TOKEN (QUEST);  }
{ID}          {
                  misc::symbol valu(yytext);
                  return TOKEN_VAL (ID, valu);
              }
{SPACE}       { tp.location_.columns(); }
{EOL}         {
                tp.location_.lines();
                tp.location_.step();
              }
.             {
                    tp.error_ << misc::error::scan
                          << tp.location_
                          << ERROR
                          << "unexpected character: "
                          << yytext
                          << std::endl;
              }

%%

// Do not use %option noyywrap, because then flex generates the same
// definition of yywrap, but outside the namespaces, so it defines it
// for ::yyFlexLexer instead of ::parse::yyFlexLexer.
int yyFlexLexer::yywrap() { return 1; }

void
yyFlexLexer::scan_open_(std::istream& f)
{
  yypush_buffer_state(YY_CURRENT_BUFFER);
  yy_switch_to_buffer(yy_create_buffer(&f, YY_BUF_SIZE));
}

void
yyFlexLexer::scan_close_()
{
  yypop_buffer_state();
}

YY_FLEX_NAMESPACE_END
