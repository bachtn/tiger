/**
 ** \file escapes/escapes-visitor.cc
 ** \brief Implementation for escapes/escapes-visitor.hh.
 */

#include <misc/contract.hh>

#include <ast/all.hh>
#include <escapes/escapes-visitor.hh>

namespace escapes
{
  // FIXME: Some code was deleted here.
  void
  EscapesVisitor::operator()(ast::FunctionDec& e)
  {
    ++depth_;
    super_type::operator()(e);
    --depth_;
  }

  void
  EscapesVisitor::operator()(ast::VarDec& e)
  {
    var_dec_map_.insert(std::make_pair(&e, depth_));
    e.escapable_set(false);
    super_type::operator()(e);
  }

  void
  EscapesVisitor::operator()(ast::SimpleVar& e)
  {
    int var_depth = var_dec_map_.find(e.def_get())->second;
    bool escape_var = e.def_get()->escapable_get();
    if (!escape_var && var_depth < depth_)
      e.def_get()->escapable_set(true);
  }
}
