/** \file misc/scoped-map.hxx
 ** \brief Implementation of misc::scoped_map.
 */

#pragma once

#include <stdexcept>
#include <sstream>
#include <type_traits>

#include <misc/contract.hh>
#include <misc/algorithm.hh>
#include <misc/indent.hh>
#include <iostream>

namespace misc
{
  template <typename Key, typename Data>
  scoped_map<Key, Data>::scoped_map()
  : scopes_ (1)
  {}

  template <typename Key, typename Data>
  void scoped_map<Key, Data>::put(const Key& key, const Data& value)
  {
    scopes_.back()[key] = value;
  }

  template <typename Key, typename Data>
  Data scoped_map<Key, Data>::get(const Key& key) const
  {
    auto it = scopes_.back().find(key);
    if (it != scopes_.back().end())
      return it->second;
    else
      return get_traits<Data>();
  }

  template<typename Data>
  typename std::enable_if<std::is_pointer<Data>::value, Data>::type
  get_traits()
  {
    return nullptr;
  }

  template<typename Data>
  typename std::enable_if<!std::is_pointer<Data>::value, Data>::type
  get_traits()
  {
    throw std::range_error("Range error\n");
  }

  template <typename Key, typename Data>
  std::ostream& scoped_map<Key, Data>::dump (std::ostream& ostr) const
  {
      for (const auto& scope:scopes_)
      {
        ostr << "Begin scope \n";
        for (const auto& elt:scope)
        {
             ostr << "key : " << elt.first
                 << " data :  " << elt.second
                 << "\n";
        }
        ostr << "Ending scope \n";
      }
    return ostr;
  }

  template <typename Key, typename Data>
  void scoped_map<Key, Data>::scope_begin()
  {
      scopes_.push_back(scopes_.back());
  }

  template <typename Key, typename Data>
  void scoped_map<Key, Data>::scope_end()
  {
    scopes_.pop_back();
  }

  template <typename Key, typename Data>
  inline std::ostream&
  operator<<(std::ostream& ostr, const scoped_map<Key, Data>& tbl)
  {
    return tbl.dump(ostr);
  }

} // namespace misc
