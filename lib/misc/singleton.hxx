#include "singleton.hh"
namespace misc
{
  template <typename T>
    const T& Singleton<T>::instance()
    {
      static T instance;
      return instance;
    }
}
