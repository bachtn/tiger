#pragma once

namespace misc
{
  template <typename T>
  class Singleton
  {
    public:
      Singleton(const Singleton<T>&) = delete;
      Singleton<T>& operator=(const Singleton<T>&) = delete;
      static const T& instance();
    protected:
     Singleton() = default;
  };
}

#include <misc/singleton.hxx>
