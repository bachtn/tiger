/**
 ** \file misc/symbol.hxx
 ** \brief Inline implementation of misc::symbol.
 */

#pragma once

#include <misc/symbol.hh>

namespace misc
{
  inline symbol&
  symbol::operator=(const symbol& rhs)
  {
    unique::obj_ = rhs.unique::obj_;
    return *this;
  }

  inline bool
  symbol::operator==(const symbol& rhs) const
  {
    return unique::obj_ == rhs.unique::obj_;
  }

  inline bool
  symbol::operator!=(const symbol& rhs) const
  {
    return unique::obj_ != rhs.unique::obj_;
  }

  inline std::ostream&
  operator<<(std::ostream& ostr, const symbol& the)
  {
    return ostr << the.get();
  }
}
